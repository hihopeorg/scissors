/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lyft.ohos.scissors2.util;

import ohos.agp.animation.Animator;

public class DecelerateInterpolator implements Animator.TimelineCurve {
    private float mFactor = 1.0f;
    @Override
    public float getCurvedTime(float v) {
        float result;
        if (mFactor == 1.0f) {
            result = (float)(1.0f - (1.0f - v) * (1.0f - v));
        } else {
            result = (float)(1.0f - Math.pow((1.0f - v), 2 * mFactor));
        }
        return result;
    }
}
