/*
 * Copyright (C) 2013 Square, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lyft.ohos.scissors2.picasso;


import ohos.agp.components.element.Element;
import ohos.global.resource.NotExistException;
import ohos.global.resource.WrongTypeException;
import ohos.media.image.PixelMap;
import java.io.IOException;

final class TargetAction extends Action<Target> {


    public TargetAction(Picasso picasso, Target target, Request request, int memoryPolicy, int networkPolicy, Element errorDrawable, String requestKey, Object tag, int errorResId) {
        super(picasso, target, request, memoryPolicy, networkPolicy, errorResId, errorDrawable, requestKey, tag,
                false);    }

    @Override
    void complete(PixelMap result, Picasso.LoadedFrom from) {
        if (result == null) {
            throw new AssertionError(
                String.format("Attempted to complete action with no result!\n%s", this));
        }
        Target target = getTarget();
        if (target != null) {
            target.onBitmapLoaded(result, from);
            if (result.isReleased()) {
                throw new IllegalStateException("Target callback must not recycle bitmap!");
            }
        }
    }

    @Override
    void error(Exception e) {
        Target target = getTarget();
        if (target != null) {
            if (errorResId != 0) {
                try {
                    target.onBitmapFailed(e, picasso.context.getResourceManager().getElement(errorResId));
                } catch (IOException ex) {
                    ex.printStackTrace();
                } catch (NotExistException ex) {
                    ex.printStackTrace();
                } catch (WrongTypeException ex) {
                    ex.printStackTrace();
                }
            } else {
                target.onBitmapFailed(e, errorDrawable);
            }
        }

    }
}
