/*
 * Copyright (C) 2015 Lyft, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lyft.ohos.scissors2;


import ohos.agp.components.AttrSet;
import ohos.app.Context;

public class CropViewConfig {

    public static final float DEFAULT_VIEWPORT_RATIO = 1f;
    public static final float DEFAULT_MAXIMUM_SCALE = 10f;
    public static final float DEFAULT_MINIMUM_SCALE = 0f;
    public static final int DEFAULT_IMAGE_QUALITY = 100;
    public static final int DEFAULT_VIEWPORT_OVERLAY_PADDING = 0;
    public static final int DEFAULT_VIEWPORT_OVERLAY_COLOR = 0xC8000000; // Black with 200 alpha
    public static final int DEFAULT_SHAPE = CropView.Shape.RECTANGLE;

    private float viewportRatio = DEFAULT_VIEWPORT_RATIO;
    private float maxScale = DEFAULT_MAXIMUM_SCALE;
    private float minScale = DEFAULT_MINIMUM_SCALE;
    private int viewportOverlayPadding = DEFAULT_VIEWPORT_OVERLAY_PADDING;
    private int viewportOverlayColor = DEFAULT_VIEWPORT_OVERLAY_COLOR;
    private @CropView.Shape int shape = DEFAULT_SHAPE;

    public int getViewportOverlayColor() {
        return viewportOverlayColor;
    }

    public void setViewportOverlayColor(int viewportOverlayColor) {
        this.viewportOverlayColor = viewportOverlayColor;
    }

    public int getViewportOverlayPadding() {
        return viewportOverlayPadding;
    }

    public void setViewportOverlayPadding(int viewportOverlayPadding) {
        this.viewportOverlayPadding = viewportOverlayPadding;
    }

    public float getViewportRatio() {
        return viewportRatio;
    }

    public void setViewportRatio(float viewportRatio) {
        this.viewportRatio = viewportRatio <= 0 ? DEFAULT_VIEWPORT_RATIO : viewportRatio;
    }

    public float getMaxScale() {
        return maxScale;
    }

    public void setMaxScale(float maxScale) {
        this.maxScale = maxScale <= 0 ? DEFAULT_MAXIMUM_SCALE : maxScale;
    }

    public float getMinScale() {
        return minScale;
    }

    public void setMinScale(float minScale) {
        this.minScale = minScale <= 0 ? DEFAULT_MINIMUM_SCALE : minScale;
    }

    public @CropView.Shape int shape() {
        return shape;
    }

    public void setShape(@CropView.Shape int shape) {
        this.shape = shape;
    }

    public static CropViewConfig from(Context context, AttrSet attrs) {
        final CropViewConfig cropViewConfig = new CropViewConfig();

        if (attrs == null) {
            return cropViewConfig;
        }

        cropViewConfig.setViewportRatio(CropViewConfig.DEFAULT_VIEWPORT_RATIO);

        cropViewConfig.setMaxScale(CropViewConfig.DEFAULT_MAXIMUM_SCALE);

        cropViewConfig.setMinScale(CropViewConfig.DEFAULT_MINIMUM_SCALE);

        cropViewConfig.setViewportOverlayColor(CropViewConfig.DEFAULT_VIEWPORT_OVERLAY_COLOR);

        cropViewConfig.setViewportOverlayPadding(CropViewConfig.DEFAULT_VIEWPORT_OVERLAY_PADDING);

        @CropView.Shape int shape = CropViewConfig.DEFAULT_SHAPE;
        cropViewConfig.setShape(shape);


        return cropViewConfig;
    }
}
