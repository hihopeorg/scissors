/*
 * Copyright (C) 2013 Square, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lyft.ohos.scissors2.picasso;


import ohos.aafwk.ability.DataAbilityHelper;
import ohos.aafwk.ability.DataAbilityRemoteException;
import ohos.app.Context;

import ohos.media.image.PixelMap;
import ohos.media.photokit.metadata.AVMetadataHelper;

import okio.Okio;
import okio.Source;

import java.io.FileDescriptor;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.FileInputStream;

import static com.lyft.ohos.scissors2.picasso.Picasso.LoadedFrom.DISK;


class ContentStreamRequestHandler extends RequestHandler {
  private static final String SCHEME_CONTENT ="dataability";
  private static final String MEDIA_VIDEO = "video";

  final Context context;

  ContentStreamRequestHandler(Context context) {
    this.context = context;
  }

  @Override public boolean canHandleRequest(Request data) {
    return SCHEME_CONTENT.equals(data.uri.getScheme());
  }

  @Override public Result load(Request request, int networkPolicy) throws IOException, DataAbilityRemoteException {
    Source source = null;
    
    if (request.uri.getDecodedPath().contains(MEDIA_VIDEO)) {
      FileDescriptor fileDescriptor = null;
      AVMetadataHelper metadataHelper = new AVMetadataHelper();
      PixelMap pixelMap = null;
      try {
        //Set the AVMetadataHelper's source using fileDescriptor and fetch the PixelMap frame
        fileDescriptor = DataAbilityHelper.creator(context).openFile(request.uri, "r");
        metadataHelper.setSource(fileDescriptor);
        pixelMap  = metadataHelper.fetchVideoPixelMapByTime();
      } catch (DataAbilityRemoteException | FileNotFoundException exception) {
        exception.printStackTrace();
      }
      metadataHelper.release(); //Release the media resources that have been read already using AVMetadataHelper
      if (pixelMap != null) {
        return new Result(pixelMap, DISK);
      }
    }

    try {
      source = Okio.source(getInputStream(request));
    } catch (DataAbilityRemoteException e) {
      e.printStackTrace();
    }
    return new Result(source, DISK);
  }

  public InputStream getInputStream(Request request) throws FileNotFoundException, DataAbilityRemoteException {
    DataAbilityHelper da=DataAbilityHelper.creator(context);

    FileDescriptor fd=null;
    try {
      fd= da.openFile(request.uri,"r");

    }catch (Exception e){

    }
    return new FileInputStream(fd);
  }
}
