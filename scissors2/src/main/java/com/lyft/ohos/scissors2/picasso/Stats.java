/*
 * Copyright (C) 2013 Square, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lyft.ohos.scissors2.picasso;


import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.eventhandler.InnerEvent;
import ohos.media.image.PixelMap;

public class Stats {
    private static final int CACHE_HIT = 0;
    private static final int CACHE_MISS = 1;
    private static final int BITMAP_DECODE_FINISHED = 2;
    private static final int BITMAP_TRANSFORMED_FINISHED = 3;
    private static final int DOWNLOAD_FINISHED = 4;

    private static final String STATS_THREAD_NAME = Utils.THREAD_PREFIX + "Stats";

    final EventRunner statsThread;
    final Cache cache;
    final EventHandler handler;
    private InnerEvent handler_Inner_Event;

    long cacheHits;
    long cacheMisses;
    long totalDownloadSize;
    long totalOriginalBitmapSize;
    long totalTransformedBitmapSize;
    long averageDownloadSize;
    long averageOriginalBitmapSize;
    long averageTransformedBitmapSize;
    int downloadCount;
    int originalBitmapCount;
    int transformedBitmapCount;

    Stats(Cache cache) {
        this.cache = cache;
        this.statsThread = EventRunner.create(STATS_THREAD_NAME);
        Utils.flushStackLocalLeaks(statsThread);
        this.handler = new StatsHandler(statsThread, this);
    }

    void dispatchBitmapDecoded(PixelMap bitmap) {
        processBitmap(bitmap, BITMAP_DECODE_FINISHED);
    }

    void dispatchBitmapTransformed(PixelMap bitmap) {
        processBitmap(bitmap, BITMAP_TRANSFORMED_FINISHED);
    }

    void dispatchDownloadFinished(long size) {
        handler.sendEvent(handler_Inner_Event.get(DOWNLOAD_FINISHED, size));
    }

    void dispatchCacheHit() {
        handler.sendEvent(CACHE_HIT);
    }

    void dispatchCacheMiss() {
        handler.sendEvent(CACHE_MISS);
    }

    void shutdown() {
        statsThread.stop();
    }

    void performCacheHit() {
        cacheHits++;
    }

    void performCacheMiss() {
        cacheMisses++;
    }

    void performDownloadFinished(Long size) {
        downloadCount++;
        totalDownloadSize += size;
        averageDownloadSize = getAverage(downloadCount, totalDownloadSize);
    }

    void performBitmapDecoded(long size) {
        originalBitmapCount++;
        totalOriginalBitmapSize += size;
        averageOriginalBitmapSize = getAverage(originalBitmapCount, totalOriginalBitmapSize);
    }

    void performBitmapTransformed(long size) {
        transformedBitmapCount++;
        totalTransformedBitmapSize += size;
        averageTransformedBitmapSize = getAverage(originalBitmapCount, totalTransformedBitmapSize);
    }

    StatsSnapshot createSnapshot() {
        return new StatsSnapshot(cache.maxSize(), cache.size(), cacheHits, cacheMisses,
            totalDownloadSize, totalOriginalBitmapSize, totalTransformedBitmapSize, averageDownloadSize,
            averageOriginalBitmapSize, averageTransformedBitmapSize, downloadCount, originalBitmapCount,
            transformedBitmapCount, System.currentTimeMillis());
    }

    private void processBitmap(PixelMap bitmap, int what) {
        // Never send bitmaps to the handler as they could be recycled before we process them.
        int bitmapSize = Utils.getBitmapBytes(bitmap);
        handler.sendEvent(handler_Inner_Event.get(what, bitmapSize, 0));
    }

    private static long getAverage(int count, long totalSize) {
        return totalSize / count;
    }

    private static class StatsHandler extends EventHandler {

        private final Stats stats;

        StatsHandler(EventRunner looper, Stats stats) {
            super(looper);
            this.stats = stats;
        }

        public void handleMessage(final InnerEvent msg) {
            switch (msg.eventId) {
                case CACHE_HIT:
                    stats.performCacheHit();
                    break;
                case CACHE_MISS:
                    stats.performCacheMiss();
                    break;
                case BITMAP_DECODE_FINISHED:
                    stats.performBitmapDecoded(msg.param);
                    break;
                case BITMAP_TRANSFORMED_FINISHED:
                    stats.performBitmapTransformed(msg.param);
                    break;
                case DOWNLOAD_FINISHED:
                    stats.performDownloadFinished((Long) msg.object);
                    break;
                default:
                    Picasso.HANDLER.postTask(new Runnable() {
                        @Override public void run() {
                            throw new AssertionError("Unhandled stats message.");
                        }
                    });
            }
        }
    }
}
