/*
 * Copyright (C) 2015 Lyft, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lyft.ohos.scissors2;


import ohos.agp.components.element.Element;
import ohos.agp.render.Canvas;
import ohos.agp.render.Texture;
import ohos.agp.render.render3d.BuildConfig;
import ohos.agp.utils.Rect;
import ohos.media.image.ImagePacker;
import ohos.media.image.PixelMap;
import ohos.media.image.common.Size;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import static ohos.media.image.common.PixelFormat.ARGB_8888;

class Utils {

    public static void checkArg(boolean expression, String msg) {
        if (!expression) {
            throw new IllegalArgumentException(msg);
        }
    }

    public static void checkNotNull(Object object, String msg) {
        if (object == null) {
            throw new NullPointerException(msg);
        }
    }

    public static PixelMap asBitmap(Element drawable, int minWidth, int minHeight) {
        final Rect tmpRect = new Rect();
        drawable.setBounds(tmpRect);
        if (tmpRect.isEmpty()) {
            tmpRect.set(0, 0, Math.max(minWidth, drawable.getWidth()), Math.max(minHeight, drawable.getHeight()));
            drawable.setBounds(tmpRect);
        }
        PixelMap.InitializationOptions initializationOptions = new PixelMap.InitializationOptions();
        initializationOptions.size = new Size(tmpRect.getWidth(),tmpRect.getHeight());
        initializationOptions.pixelFormat = ARGB_8888;
        PixelMap bitmap = PixelMap.create(initializationOptions);
        drawable.drawToCanvas(new Canvas(new Texture(bitmap)));
        return bitmap;
    }

    private final static ExecutorService EXECUTOR_SERVICE = Executors.newCachedThreadPool();
    private static final String TAG = "scissors.Utils";

    public static Future<Void> flushToFile(final PixelMap bitmap,
            final String format,
            final int quality,
            final File file) {

        return EXECUTOR_SERVICE.submit(new Runnable() {
            @Override
            public void run() {
                OutputStream outputStream = null;

                try {
                    file.getParentFile().mkdirs();
                    outputStream = new FileOutputStream(file);

                    compress(bitmap,format, quality, outputStream);
                    outputStream.flush();
                } catch (final Throwable throwable) {
                    if (BuildConfig.DEBUG) {

                    }
                } finally {
                    closeQuietly(outputStream);
                }
            }
        }, null);
    }

    public static Future<Void> flushToStream(final PixelMap bitmap,
            final String format,
            final int quality,
            final OutputStream outputStream,
            final boolean closeWhenDone) {

        return EXECUTOR_SERVICE.submit(new Runnable() {
            @Override
            public void run() {
                try {
                    compress(bitmap,format, quality, outputStream);
                    outputStream.flush();
                } catch (final Throwable throwable) {
                } finally {
                    if (closeWhenDone) {
                        closeQuietly(outputStream);
                    }
                }
            }
        }, null);
    }

    private static void closeQuietly(OutputStream outputStream) {
        try {
            if (outputStream != null) {
                outputStream.close();
            }
        } catch (Exception e) {
        }
    }

    public static boolean compress(PixelMap pixelMap,String compressFormat,int quality,OutputStream outputStream){
        if (pixelMap == null){
            return false;
        }

        ImagePacker imagePacker = ImagePacker.create();
        ImagePacker.PackingOptions packingOptions = new ImagePacker.PackingOptions();
        packingOptions.format = compressFormat;
        packingOptions.quality = quality;
        boolean result = imagePacker.initializePacking(outputStream,packingOptions);
        if(result){
            imagePacker.addImage(pixelMap);
            imagePacker.finalizePacking();
            return true;
        }else {
            return false;
        }
    }
}
