/*
 * Copyright (C) 2013 Square, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lyft.ohos.scissors2.picasso;


import ohos.agp.components.ComponentProvider;
import ohos.app.Context;
import ohos.event.notification.NotificationHelper;
import ohos.event.notification.NotificationRequest;
import ohos.global.resource.NotExistException;
import ohos.global.resource.RawFileEntry;
import ohos.global.resource.Resource;
import ohos.global.resource.WrongTypeException;
import ohos.global.resource.ResourceManager;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;
import ohos.rpc.RemoteException;


import java.io.IOException;

abstract class RemoteViewsAction extends Action<RemoteViewsAction.RemoteViewsTarget> {
    private static final String TAG = "RemoteViewsAction";

    final ComponentProvider remoteViews;
    final int viewId;
    Callback callback;
    Context context;

    private RemoteViewsTarget target;
    private PixelMap pixelmap;

    RemoteViewsAction(Picasso picasso, Request data, ComponentProvider remoteViews, int viewId,
                      int errorResId, int memoryPolicy, int networkPolicy, Object tag, String key,
                      Callback callback) {
        super(picasso, null, data, memoryPolicy, networkPolicy, errorResId, null, key, tag, false);
        this.remoteViews = remoteViews;
        this.viewId = viewId;
        this.callback = callback;
        this.context = Picasso.get().context;
    }
    @Override
    void complete(PixelMap result, Picasso.LoadedFrom from)  {
        remoteViews.setImagePixelMap(viewId, result);
        try {
            update(result);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        if (callback != null) {
            callback.onSuccess();
        }
    }

    @Override void cancel() {
        super.cancel();
        if (callback != null) {
            callback = null;
        }
    }


    void error(Exception e)  {
        if (errorResId != 0) {
            try {
                setImageResource(errorResId);
            } catch (RemoteException | IOException ex) {
                ex.printStackTrace();
            }
        }
        if (callback != null) {
            callback.onError(e);
        }
    }

    @Override RemoteViewsTarget getTarget() {
        if (target == null) {
            target = new RemoteViewsTarget(remoteViews, viewId);
        }
        return target;
    }

    void setImageResource(int resId) throws RemoteException, IOException {

        String path = getPathById(context,resId);
        RawFileEntry assetManager = context.getResourceManager().getRawFileEntry(path);
        ImageSource.SourceOptions options = new ImageSource.SourceOptions();
        options.formatHint = "image/png";
        ImageSource.DecodingOptions decodingOptions = new ImageSource.DecodingOptions();
        Resource asset = assetManager.openRawFile();
        ImageSource source = ImageSource.create(asset, options);
        pixelmap = source.createPixelmap(decodingOptions);

        remoteViews.setImagePixelMap(viewId,pixelmap);

        update(pixelmap);
    }

    protected abstract void update(PixelMap pixelmap) throws RemoteException;

    public static String getPathById(Context context, int id) {
        String path = "";
        if (context == null) {
            return path;
        }
        ResourceManager manager = context.getResourceManager();
        if (manager == null) {
            return path;
        }
        try {
            path = manager.getMediaPath(id);
        } catch (IOException e) {
            LogUtil.error(TAG, "getPathById -> IOException");
        } catch (NotExistException e) {
             LogUtil.error(TAG, "getPathById -> NotExistException");
        } catch (WrongTypeException e) {
             LogUtil.error(TAG, "getPathById -> WrongTypeException");
        }
        return path;
    }

    abstract void update() throws RemoteException;

    static class RemoteViewsTarget {
        final ComponentProvider remoteViews;
        final int viewId;

        RemoteViewsTarget(ComponentProvider remoteViews, int viewId) {
            this.remoteViews = remoteViews;
            this.viewId = viewId;
        }

        @Override public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            RemoteViewsTarget remoteViewsTarget = (RemoteViewsTarget) o;
            return viewId == remoteViewsTarget.viewId && remoteViews.equals(
                remoteViewsTarget.remoteViews);
        }

        @Override public int hashCode() {
            return 31 * remoteViews.hashCode() + viewId;
        }
    }

    static class AppWidgetAction extends RemoteViewsAction {
        private final int[] appWidgetIds;

        AppWidgetAction(Picasso picasso, Request data, ComponentProvider remoteViews, int viewId,
                        int[] appWidgetIds, int memoryPolicy, int networkPolicy, String key, Object tag,
                        int errorResId, Callback callback) {
            super(picasso, data, remoteViews, viewId, errorResId, memoryPolicy, networkPolicy, tag, key,
                callback);
            this.appWidgetIds = appWidgetIds;
        }

        @Override
        protected void update(PixelMap pixelmap) throws RemoteException {
        }

        @Override void update() {
        }
    }

    static class NotificationAction extends RemoteViewsAction {
        private final int notificationId;
        private final String notificationTag;
        private final NotificationRequest notification;

        NotificationAction(Picasso picasso, Request data, ComponentProvider remoteViews, int viewId,
                           int notificationId, NotificationRequest notification, String notificationTag, int memoryPolicy,
                           int networkPolicy, String key, Object tag, int errorResId, Callback callback) {
            super(picasso, data, remoteViews, viewId, errorResId, memoryPolicy, networkPolicy, tag, key,
                callback);
            this.notificationId = notificationId;
            this.notificationTag = notificationTag;
            this.notification = notification;
        }

        public void update(PixelMap pixelMap) throws RemoteException {
            NotificationRequest.NotificationPictureContent pictureContent = (NotificationRequest.NotificationPictureContent) notification.getContent().getNotificationContent();
            pictureContent.setBigPicture(pixelMap);
            NotificationHelper.publishNotification(notification);
        }

        @Override
        void update() throws RemoteException {

        }
    }
}
