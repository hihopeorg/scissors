/*
 * Copyright (C) 2013 Square, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lyft.ohos.scissors2.picasso;


import ohos.aafwk.ability.DataAbilityRemoteException;

import ohos.agp.components.ComponentProvider;
import ohos.agp.components.Image;
import ohos.agp.components.element.Element;
import ohos.agp.utils.LayoutAlignment;

import ohos.event.notification.NotificationRequest;

import ohos.media.image.PixelMap;
import ohos.media.image.common.PixelFormat;
import ohos.rpc.RemoteException;
import ohos.utils.net.Uri;


import java.io.IOException;
import java.util.concurrent.atomic.AtomicInteger;

import static com.lyft.ohos.scissors2.picasso.BitmapHunter.forRequest;
import static com.lyft.ohos.scissors2.picasso.MemoryPolicy.shouldReadFromMemoryCache;
import static com.lyft.ohos.scissors2.picasso.Picasso.LoadedFrom.MEMORY;
import static com.lyft.ohos.scissors2.picasso.PicassoDrawable.setBitmap;
import static com.lyft.ohos.scissors2.picasso.PicassoDrawable.setPlaceholder;
import static com.lyft.ohos.scissors2.picasso.Utils.OWNER_MAIN;
import static com.lyft.ohos.scissors2.picasso.Utils.VERB_CHANGED;
import static com.lyft.ohos.scissors2.picasso.Utils.VERB_COMPLETED;
import static com.lyft.ohos.scissors2.picasso.Utils.VERB_CREATED;
import static com.lyft.ohos.scissors2.picasso.Utils.checkMain;
import static com.lyft.ohos.scissors2.picasso.Utils.checkNotMain;
import static com.lyft.ohos.scissors2.picasso.Utils.createKey;
import static com.lyft.ohos.scissors2.picasso.Utils.log;


public class RequestCreator {

    private final Picasso picasso;
    private final Request.Builder data;
    private boolean deferred;
    private static final AtomicInteger nextId = new AtomicInteger();
    private int memoryPolicy;
    private boolean noFade;
    private int networkPolicy;
    private int errorResId;
    private Element errorDrawable;
    private Object tag;
    private boolean setPlaceholder = true;
    private Element placeholderDrawable;
    private int placeholderResId;

    /**
     * Instantiates a new Request creator.
     *
     * @param picasso    the picasso
     * @param uri        the uri value
     * @param resourceId the resource id value
     */
    RequestCreator(Picasso picasso, Uri uri, int resourceId) {
        if (picasso.shutdown) {
            throw new IllegalStateException(
                    "Picasso instance already shut down. Cannot submit new requests.");
        }
        this.picasso = picasso;
        this.data = new Request.Builder(uri, resourceId, picasso.defaultBitmapConfig);
    }

    /**
     * Instantiates a new Request creator.
     */
    RequestCreator() {
        this.picasso = null;
        this.data = new Request.Builder(null, 0, null);
    }

    /**
     * A placeholder drawable to be used while the image is being loaded. If the requested image is
     * not immediately available in the memory cache then this resource will be set on the target.
     *
     * @param placeholderResId the placeholder res id value
     * @return the request creator
     */
    public RequestCreator placeholder(int placeholderResId) {
        if (!setPlaceholder) {
            throw new IllegalStateException("Already explicitly declared as no placeholder.");
        }
        if (placeholderResId == 0) {
            throw new IllegalArgumentException("Placeholder image resource invalid.");
        }
        if (placeholderDrawable != null) {
            throw new IllegalStateException("Placeholder image already set.");
        }
        this.placeholderResId = placeholderResId;
        return this;
    }

    /**
     * A placeholder drawable to be used while the image is being loaded. If the requested image is
     * not immediately available in the memory cache then this resource will be set on the target
     * { ImageView}.
     * <p>
     * If you not using a placeholder image but want to clear an existing image pass in {@code null}
     *
     * @param placeholderDrawable the placeholder drawable element
     * @return the request creator
     * <p>
     * <em>Note:</em> This method keeps a weak reference to the {@link Target} instance and will be
     * garbasge collected if you do not keep a strong reference to it. To receive callbacks when an
     * mage is loaded use {#into(ohos.agp.components.Image, Callback)}}
     */
    public RequestCreator placeholder(Element placeholderDrawable) {
        if (!setPlaceholder) {
            throw new IllegalStateException("Already explicitly declared as no placeholder.");
        }
        if (placeholderResId != 0) {
            throw new IllegalStateException("Placeholder image already set.");
        }
        this.placeholderDrawable = placeholderDrawable;
        return this;
    }

    /**
     * Disable brief fade in of images loaded from the disk cache or network.
     *
     * @return the request creator
     */
    public RequestCreator noFade() {
        noFade = true;
        return this;
    }

    /**
     * Explicitly opt-out to having a placeholder set when calling {@code into}.
     * <p>
     * By default, Picasso will either set a supplied placeholder or clear the target
     * { ImageView} in order to ensure behavior in situations where views are recycled. This
     * method will prevent that behavior and retain any already set image.
     *
     * @return the request creator
     */
    public RequestCreator noPlaceholder() {
        if (placeholderResId != 0) {
            throw new IllegalStateException("Placeholder resource already set.");
        }
        if (placeholderDrawable != null) {
            throw new IllegalStateException("Placeholder image already set.");
        }
        setPlaceholder = false;
        return this;
    }

    /**
     * An error drawable to be used if the request image could not be loaded.
     *
     * @param errorDrawable the error drawable
     * @return the request creator
     */
    public RequestCreator error( Element errorDrawable) {
        if (errorDrawable == null) {
            throw new IllegalArgumentException("Error image may not be null.");
        }
        if (errorResId != 0) {
            throw new IllegalStateException("Error image already set.");
        }
        this.errorDrawable = errorDrawable;
        return this;
    }

    /**
     * An error drawable to be used if the request image could not be loaded.
     *
     * @param errorResId the error res id value
     * @return the request creator
     */
    public RequestCreator error(int errorResId) {
        if (errorResId == 0) {
            throw new IllegalArgumentException("Error image resource invalid.");
        }
        if (errorDrawable != null) {
            throw new IllegalStateException("Error image already set.");
        }
        this.errorResId = errorResId;
        return this;
    }

    /**
     * Crops an image inside of the bounds specified by {@link #resize(int, int)} rather than
     * distorting the aspect ratio. This cropping technique scales the image so that it fills the
     * requested bounds and then crops the extra.
     *
     * @return the request creator
     */
    public RequestCreator centerCrop() {
        data.centerCrop(LayoutAlignment.CENTER);
        return this;
    }

    /**
     * Crops an image inside of the bounds specified by {@link #resize(int, int)} rather than
     * distorting the aspect ratio. This cropping technique scales the image so that it fills the
     * requested bounds and then crops the extra, preferring the contents at {@code alignGravity}.
     *
     * @param alignGravity the align gravity
     * @return the request creator
     */
    public RequestCreator centerCrop(int alignGravity) {
        data.centerCrop(alignGravity);
        return this;
    }

    /**
     * Only resize an image if the original image size is bigger than the target size
     * specified by {@link #resize(int, int)}.
     *
     * @return the request creator
     */
    public RequestCreator onlyScaleDown() {
        data.onlyScaleDown();
        return this;
    }

    /**
     * Rotate the image by the specified degrees.
     *
     * @param degrees the degrees
     * @return the request creator
     */
    public RequestCreator rotate(float degrees) {
        data.rotate(degrees);
        return this;
    }

    /**
     * Attempt to decode the image using the specified config.
     * <p>
     * Note: This value may be ignored by { BitmapFactory}. See
     * { BitmapFactory.Options#inPreferredConfig its documentation} for more details.
     *
     * @param config the config
     * @return the request creator
     */
    public RequestCreator config( PixelFormat config) {
        data.config(config);
        return this;
    }

    /**
     * Set the priority of this request.
     * <p>
     * This will affect the order in which the requests execute but does not guarantee it.
     * By default, all requests have {@lin Priority#NORMAL} priority, except for
     * {@li #fetch()} requests, which have {@lin Priority#LOW} priority by default.
     *
     * @param priority the priority
     * @return the request creator
     */
    public RequestCreator priority(Picasso.Priority priority) {
        data.priority(priority);
        return this;
    }


    public RequestCreator memoryPolicy( MemoryPolicy policy,
                                        MemoryPolicy... additional) {
        if (policy == null) {
            throw new IllegalArgumentException("Memory policy cannot be null.");
        }
        this.memoryPolicy |= policy.index;
        if (additional == null) {
            throw new IllegalArgumentException("Memory policy cannot be null.");
        }
        if (additional.length > 0) {
            for (MemoryPolicy memoryPolicy : additional) {
                if (memoryPolicy == null) {
                    throw new IllegalArgumentException("Memory policy cannot be null.");
                }
                this.memoryPolicy |= memoryPolicy.index;
            }
        }
        return this;
    }


    public RequestCreator networkPolicy(NetworkPolicy policy,
                                        NetworkPolicy... additional) {
        if (policy == null) {
            throw new IllegalArgumentException("Network policy cannot be null.");
        }
        this.networkPolicy |= policy.index;
        if (additional == null) {
            throw new IllegalArgumentException("Network policy cannot be null.");
        }
        if (additional.length > 0) {
            for (NetworkPolicy networkPolicy : additional) {
                if (networkPolicy == null) {
                    throw new IllegalArgumentException("Network policy cannot be null.");
                }
                this.networkPolicy |= networkPolicy.index;
            }
        }
        return this;
    }

    /**
     * Synchronously fulfill this request. Must not be called from the main thread.
     * <p>
     * <em>Note</em>: The result of this operation is not cached in memory because the underlying
     * {@link Cache} implementation is not guaranteed to be thread-safe.
     *
     * @return the pixel map
     * @throws IOException the io exception
     */
    public PixelMap get() throws IOException {
        long started = System.nanoTime();
        checkNotMain();

        if (deferred) {
            throw new IllegalStateException("Fit cannot be used with get.");
        }
        if (!data.hasImage()) {
            return null;
        }

        Request finalData = createRequest(started);
        String key = createKey(finalData, new StringBuilder());

        Action action = new GetAction(picasso, finalData, memoryPolicy, networkPolicy, tag, key);
        try {
            return forRequest(picasso, picasso.dispatcher, picasso.cache, picasso.stats, action).hunt();
        } catch (DataAbilityRemoteException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Asynchronously fulfills the request without a {@lin ImageView} or {@link Target}. This is
     * useful when you want to warm up the cache with an image.
     * <p>
     * <em>Note:</em> It is safe to invoke this method from any thread.
     */
    public void fetch() {
        fetch(null);
    }


    public void fetch(Callback callback) {
        long started = System.nanoTime();

        if (deferred) {
            throw new IllegalStateException("Fit cannot be used with fetch.");
        }
        if (data.hasImage()) {
            // Fetch requests have lower priority by default.
            if (!data.hasPriority()) {
                data.priority(Picasso.Priority.LOW);
            }

            Request request = createRequest(started);
            String key = createKey(request, new StringBuilder());

            if (shouldReadFromMemoryCache(memoryPolicy)) {
                PixelMap bitmap = picasso.quickMemoryCacheCheck(key);
                if (bitmap != null) {
                    if (picasso.loggingEnabled) {
                        log(OWNER_MAIN, VERB_COMPLETED, request.plainId(), "from " + MEMORY);
                    }
                    if (callback != null) {
                        callback.onSuccess();
                    }
                    return;
                }
            }

            Action action = new FetchAction(picasso, request, memoryPolicy, networkPolicy, tag, key, callback);
            picasso.submit(action);
        }
    }


    /**
     * Sets the stable key for this request to be used instead of the URI or resource ID when
     * caching. Two requests with the same value are considered to be for the same resource.
     *
     * @param stableKey the stable key value
     * @return the request creator
     */
    public RequestCreator stableKey( String stableKey) {
        data.stableKey(stableKey);
        return this;
    }


    /**
     * Resize the image to the specified dimension size.
     *
     * @param targetWidthResId  the target width res id value
     * @param targetHeightResId the target height res id value
     * @return the request creator
     */
    public RequestCreator resizeDimen(int targetWidthResId, int targetHeightResId) {
       int targetWidth = ResUtil.getIntDimen( picasso.context, targetWidthResId);
       int targetHeight = ResUtil.getIntDimen( picasso.context, targetHeightResId);
        return resize(targetWidth, targetHeight);
    }


    /**
     * Resize the image to the specified size in pixels.
     *
     * @param targetWidth  the target width
     * @param targetHeight the target height
     * @return the request creator
     */
    public RequestCreator resize(int targetWidth, int targetHeight) {
        data.resize(targetWidth, targetHeight);
        return this;
    }

    /**
     * Assign a tag to this request. Tags are an easy way to logically associate
     * related requests that can be managed together e.g. paused, resumed,
     * or canceled.
     * <p>
     * You can either use simple  tags or objects that naturally
     * define the scope of your requests within your app such as a
     * <strong>WARNING:</strong>: Picasso will keep a reference to the tag for
     * as long as this tag is paused and/or has active requests. Look out for
     * potential leaks.
     *
     * @param tag the tag value
     * @return the request creator
     */
    public RequestCreator tag( Object tag) {
        if (tag == null) {
            throw new IllegalArgumentException("Tag invalid.");
        }
        if (this.tag != null) {
            throw new IllegalStateException("Tag already set.");
        }
        this.tag = tag;
        return this;
    }


    /**
     * Attempt to resize the image to fit exactly into the target 's bounds. This
     * will result in delayed execution of the request until the has been laid out.
     * <p>
     * <em>Note:</em> This method works only when your target is an .
     *
     * @return the request creator
     */
    public RequestCreator fit() {
        deferred = true;
        return this;
    }


    RequestCreator unfit() {
        deferred = false;
        return this;
    }

    /**
     * Centers an image inside of the bounds specified by . This scales
     * the image so that both dimensions are equal to or less than the requested bounds.
     *
     * @return the request creator
     */
    public RequestCreator centerInside() {
        data.centerInside();
        return this;
    }


    /**
     * Asynchronously fulfills the request into the specified {@link Target}. In most cases, you
     * should use this when you are dealing with a custom {link COMPONENT} or view
     * holder which should implement the {@link Target} interface.
     * <p>
     * Implementing on a {@lnk comonent}:
     * <blockquote><pre>
     * public class ProfileView extends FrameLayout implements Target {
     *   {@literal @}Override public void onBitmapLoaded(Bitmap bitmap, LoadedFrom from) {
     *     setBackgroundDrawable(new BitmapDrawable(bitmap));
     *   }
     *
     *   {@literal @}Override public void onBitmapFailed(Exception e, Drawable errorDrawable) {
     *     setBackgroundDrawable(errorDrawable);
     *   }
     *
     *   {@literal @}Override public void onPrepareLoad(Drawable placeHolderDrawable) {
     *     setBackgroundDrawable(placeHolderDrawable);
     *   }
     * }
     * </pre></blockquote>
     * Implementing on a view holder object for use inside of an adapter:
     * <blockquote><pre>
     * public class ViewHolder implements Target {
     *   public FrameLayout frame;
     *   public TextView name;
     *
     *   {@literal @}Override public void onBitmapLoaded(Bitmap bitmap, LoadedFrom from) {
     *     frame.setBackgroundDrawable(new BitmapDrawable(bitmap));
     *   }
     *
     *   {@literal @}Override public void onBitmapFailed(Exception e, Drawable errorDrawable) {
     *     frame.setBackgroundDrawable(errorDrawable);
     *   }
     *
     *   {@literal @}Override public void onPrepareLoad(Drawable placeHolderDrawable) {
     *     frame.setBackgroundDrawable(placeHolderDrawable);
     *   }
     * }
     * </pre></blockquote>
     * <p>
     * <em>Note:</em> This method keeps a weak reference to the {@link Target} instance and will be
     * garbage collected if you do not keep a strong reference to it. To receive callbacks when an
     * image is loaded use {#into(ohos.agp.components.Image, Callback)}.
     *
     * @param target the target
     */
    public void into( Target target) {
        long started = System.nanoTime();
        checkMain();

        if (target == null) {
            throw new IllegalArgumentException("Target must not be null.");
        }
        if (deferred) {
            throw new IllegalStateException("Fit cannot be used with a Target.");
        }

        if (!data.hasImage()) {
            picasso.cancelRequest(target);
            target.onPrepareLoad(setPlaceholder ? getPlaceholderDrawable() : null);
            return;
        }

        Request request = createRequest(started);
        String requestKey = createKey(request);

        if (shouldReadFromMemoryCache(memoryPolicy)) {
            PixelMap bitmap = picasso.quickMemoryCacheCheck(requestKey);
            if (bitmap != null) {
                picasso.cancelRequest(target);
                target.onBitmapLoaded(bitmap, MEMORY);
                return;
            }
        }

        target.onPrepareLoad(setPlaceholder ? getPlaceholderDrawable() : null);

        Action action = new TargetAction(picasso, target, request, memoryPolicy, networkPolicy, errorDrawable, requestKey, tag, errorResId);
        picasso.enqueueAndSubmit(action);
    }

    /**
     * Asynchronously fulfills the request into the specified object with the
     * given {@code viewId}. This is used for loading bitmaps into a notification.
     *
     * @param remoteViews    the remote views
     * @param viewId         the view id value
     * @param notificationId the notification id value
     * @param notification   the notification
     * @throws RemoteException the remote exception
     * @throws IOException     the io exception
     */
    public void into(ComponentProvider remoteViews, int viewId, int notificationId,
                     NotificationRequest notification) throws RemoteException, IOException {
        into(remoteViews, viewId, notificationId, notification, null);
    }

    /**
     * Asynchronously fulfills the request into the specified  object with the
     * given {@code viewId}. This is used for loading bitmaps into Notification.
     *
     * @param remoteViews     the remote views
     * @param viewId          the view id value
     * @param notificationId  the notification id value
     * @param notification    the notification
     * @param notificationTag the notification tag value
     * @throws RemoteException the remote exception
     * @throws IOException     the io exception
     */
    public void into(ComponentProvider remoteViews,int viewId, int notificationId,
                     NotificationRequest notification,String notificationTag) throws RemoteException, IOException {
        into(remoteViews, viewId, notificationId, notification, notificationTag, null);
    }

    /**
     * Asynchronously fulfills the request into the specified  object with the
     * given {@code viewId}. This is used for loading bitmaps into  Notification.
     *
     * @param remoteViews     the remote views
     * @param viewId          the view id value
     * @param notificationId  the notification id value
     * @param notification    the notification
     * @param notificationTag the notification tag value
     * @param callback        the callback
     * @throws RemoteException the remote exception
     * @throws IOException     the io exception
     */
    public void into(ComponentProvider remoteViews, int viewId, int notificationId,
                     NotificationRequest notification,String notificationTag, Callback callback) throws RemoteException, IOException {
        long started = System.nanoTime();

        if (remoteViews == null) {
            throw new IllegalArgumentException("RemoteViews must not be null.");
        }
        if (notification == null) {
            throw new IllegalArgumentException("Notification must not be null.");
        }
        if (deferred) {
            throw new IllegalStateException("Fit cannot be used with RemoteViews.");
        }
        if (placeholderDrawable != null || placeholderResId != 0 || errorDrawable != null) {
            throw new IllegalArgumentException(
                    "Cannot use placeholder or error drawables with remote views.");
        }
        Request request = createRequest(started);
        String key = createKey(request, new StringBuilder()); // Non-main thread needs own builder.

        RemoteViewsAction action =
                new RemoteViewsAction.NotificationAction(picasso, request, remoteViews, viewId, notificationId, notification,
                        notificationTag, memoryPolicy, networkPolicy, key, tag, errorResId, callback);

        performRemoteViewInto(action);
    }

    private Element getPlaceholderDrawable()  {
        return placeholderDrawable;
    }


    /**
     * Asynchronously fulfills the request into the specified {@link Image}.
     * <p>
     * <em>Note:</em> This method keeps a weak reference to the {@link Image} instance and will
     * automatically support object recycling.
     *
     * @param target the target
     */
    public void into(Image target) {
        into(target, null);
    }


    public void into(Image target, Callback callback) {
        long started = System.nanoTime();
        checkMain();

        if (target == null) {
            throw new IllegalArgumentException("Target must not be null.");
        }

        if (!data.hasImage()) {
            picasso.cancelRequest(target);
            if (setPlaceholder) {
                setPlaceholder(target, placeholderResId);
            }
            return;
        }

        if (deferred) {
            if (data.hasSize()) {
                throw new IllegalStateException("Fit cannot be used with resize.");
            }

            int width = target.getWidth();
            int height = target.getHeight();

            if (width == 0 || height == 0) {

                if (setPlaceholder) {
                    setPlaceholder(target, placeholderResId);
                }
                picasso.defer(target, new DeferredRequestCreator(this, target, callback));

                return;
            }
            data.resize(width, height);
        }

        Request request = createRequest(started);
        String requestKey = createKey(request);

        if (shouldReadFromMemoryCache(memoryPolicy)) {
            PixelMap bitmap = picasso.quickMemoryCacheCheck(requestKey);

            if (bitmap != null) {
                picasso.cancelRequest(target);
                setBitmap(target, picasso.context, bitmap, MEMORY, noFade, picasso.indicatorsEnabled, data.build());

                if (picasso.loggingEnabled) {
                   log(OWNER_MAIN, VERB_COMPLETED, request.plainId(), "from " + MEMORY);
               }
                if (callback != null) {
                    callback.onSuccess();
                }
                return;
            }
        }

       if (setPlaceholder) {
           setPlaceholder(target, placeholderResId);
       }
       Action action = new ImageViewAction(picasso, target, request, memoryPolicy, networkPolicy, errorResId, errorDrawable, requestKey, tag, callback, noFade);
       picasso.enqueueAndSubmit(action);
    }

    /** Create the request optionally passing it through the request transformer.
     * @param started the started
     * @return the request creator
     * */
    private Request createRequest(long started) {
        int id = nextId.getAndIncrement();

        Request request = data.build();
        request.id = id;
        request.started = started;

        boolean loggingEnabled = picasso.loggingEnabled;
        if (loggingEnabled) {
             log(OWNER_MAIN, VERB_CREATED, request.plainId(), request.toString());
        }

        Request transformed = picasso.transformRequest(request);
        if (transformed != request) {
            // If the request was changed, copy over the id and timestamp from the original.
            transformed.id = id;
            transformed.started = started;

            if (loggingEnabled) {
                 log(OWNER_MAIN, VERB_CHANGED, transformed.logId(), "into " + transformed);
            }
        }
        return transformed;
    }

    /**
     * Internal use only. Used by {@link DeferredRequestCreator}.
     *
     * @return the request creator
     */
    RequestCreator clearTag() {
        this.tag = null;
        return this;
    }

    /**
     * Gets tag.
     *
     * @return the tag value
     */
    Object getTag() {
        return tag;
    }

    private void performRemoteViewInto(RemoteViewsAction action) throws RemoteException, IOException {
        if (shouldReadFromMemoryCache(memoryPolicy)) {
            PixelMap bitmap = picasso.quickMemoryCacheCheck(action.getKey());
            if (bitmap != null) {
                action.complete(bitmap, MEMORY);
                return;
            }
        }

        if (placeholderResId != 0) {
            action.setImageResource(placeholderResId);
        }

        picasso.enqueueAndSubmit(action);
    }

    public RequestCreator transform(Transformation transformation) {
        data.transform(transformation);
        return this;
    }
}
