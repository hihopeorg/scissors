/*
 * Copyright (C) 2013 Square, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lyft.ohos.scissors2.picasso;

import java.io.IOException;


import ohos.app.Context;
import ohos.global.resource.ResourceManager;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;

import static com.lyft.ohos.scissors2.picasso.Picasso.LoadedFrom.DISK;

class ResourceRequestHandler extends RequestHandler {
  private final Context context;

  ResourceRequestHandler(Context context) {
    this.context = context;
  }

  @Override public boolean canHandleRequest(Request data) {
    if (data.resourceId != 0) {
      return true;
    }
    return true;
  }

  @Override public Result load(Request request, int networkPolicy) throws IOException {
    ResourceManager res = Utils.getResources(context, request);
    int id = Utils.getResourceId(res, request);
    return new Result(decodeResource(res, id, request), DISK);
  }

  private static PixelMap decodeResource(ResourceManager resources, int id, Request data) {
    ImageSource.DecodingOptions options = createBitmapOptions(data);
    if (requiresInSampleSize(options)) {
      calculateInSampleSize(data.targetWidth, data.targetHeight, options, data);
    }
    return null;
  }
}
