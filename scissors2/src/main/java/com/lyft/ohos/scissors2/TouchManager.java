/*
 * Copyright (C) 2015 Lyft, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lyft.ohos.scissors2;



import com.lyft.ohos.scissors2.util.AccelerateDecelerateInterpolator;
import com.lyft.ohos.scissors2.util.DecelerateInterpolator;
import com.lyft.ohos.scissors2.util.GestureDetector;
import com.lyft.ohos.scissors2.util.ScaleGestureDetector;
import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorGroup;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.Image;
import ohos.agp.components.ScrollHelper;
import ohos.agp.utils.Matrix;
import ohos.agp.utils.Rect;
import ohos.multimodalinput.event.TouchEvent;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

class TouchManager {

    private static final int MINIMUM_FLING_VELOCITY = 2500;

    private final CropViewConfig cropViewConfig;

    private final ScaleGestureDetector scaleGestureDetector;
    private final GestureDetector gestureDetector;

    private float minimumScale;
    private float maximumScale;
    private Rect imageBounds = new Rect();
    private float aspectRatio;
    private int viewportWidth;
    private int viewportHeight;
    private int bitmapWidth;
    private int bitmapHeight;

    private int verticalLimit;
    private int horizontalLimit;

    private float scale = -1.0f;
    private final TouchPoint position = new TouchPoint();

    private final Image imageView;

    private final GestureAnimator gestureAnimator = new GestureAnimator(new GestureAnimator.OnAnimationUpdateListener() {
        @Override
        public void onAnimationUpdate(@GestureAnimator.AnimationType int animationType, float animationValue) {
            if(animationType == GestureAnimator.ANIMATION_X) {
                position.set(animationValue, position.getY());
                ensureInsideViewport();
            }
            else if(animationType == GestureAnimator.ANIMATION_Y) {
                position.set(position.getX(), animationValue);
                ensureInsideViewport();
            }
            else if(animationType == GestureAnimator.ANIMATION_SCALE) {
                scale = animationValue;
                setLimits();
            }

            imageView.invalidate();
        }

        @Override
        public void onAnimationFinished() {
            ensureInsideViewport();
        }
    });

    private final ScaleGestureDetector.OnScaleGestureListener scaleGestureListener = new ScaleGestureDetector.OnScaleGestureListener() {
        @Override
        public boolean onScale(ScaleGestureDetector detector) {
            scale = calculateScale(detector.getScaleFactor());
            setLimits();
            return true;
        }

        @Override public boolean onScaleBegin(ScaleGestureDetector detector) {return true;}
        @Override public void onScaleEnd(ScaleGestureDetector detector) {}
    };

    private final GestureDetector.OnGestureListener gestureListener = new GestureDetector.SimpleOnGestureListener() {
        @Override
        public boolean onDown(TouchEvent e) {
            return true;
        }

        @Override
        public boolean onScroll(TouchEvent e1, TouchEvent e2, float distanceX, float distanceY) {
            if (e2.getPointerCount() != 1) {
                return true;
            }

            TouchPoint delta = new TouchPoint(-distanceX, -distanceY);
            position.add(delta);
            ensureInsideViewport();
            return true;
        }

        @Override
        public boolean onFling(TouchEvent e1, TouchEvent e2, float velocityX, float velocityY) {
            velocityX /= 2;
            velocityY /= 2;

            if(Math.abs(velocityX) < MINIMUM_FLING_VELOCITY) {
                velocityX = 0;
            }
            if(Math.abs(velocityY) < MINIMUM_FLING_VELOCITY) {
                velocityY = 0;
            }

            if(velocityX == 0 && velocityY == 0) {
                return true;
            }

            int width = (int) (imageBounds.right * scale);
            int height = (int) (imageBounds.bottom * scale);

//            OverScroller scroller = new OverScroller(imageView.getContext());
//            scroller.fling((int) e1.getX(), (int) e1.getY(), (int) velocityX, (int) velocityY, -width, width, -height, height);

            ScrollHelper scroller = new ScrollHelper();
            scroller.doFling((int) e1.getPointerPosition(e1.getIndex()).getX(), (int) e1.getPointerPosition(e1.getIndex()).getY(), (int) velocityX, (int) velocityY, -width, width, -height, height);

            float finalX = scroller.getScrollDistanceX()+e1.getPointerPosition(e1.getIndex()).getX();
            float finalY = scroller.getScrollDistanceY()+e1.getPointerPosition(e1.getIndex()).getY();

            TouchPoint target = new TouchPoint(finalX, finalY);
            float x = velocityX == 0 ? position.getX() : target.getX() * scale;
            float y = velocityY == 0 ? position.getY() : target.getY() * scale;

            gestureAnimator.animateTranslation(position.getX(), x, position.getY(), y);

            return true;
        }

        @Override
        public boolean onDoubleTap(TouchEvent e) {
            final float fromX, toX, fromY, toY, targetScale;

            TouchPoint eventPoint = new TouchPoint(e.getPointerPosition(e.getIndex()).getX(), e.getPointerPosition(e.getIndex()).getY());
            if(scale == minimumScale) {
                targetScale = maximumScale / 2;
                TouchPoint translatedTargetPosition = mapTouchCoordinateToMatrix(eventPoint, targetScale);
                TouchPoint centeredTargetPosition = centerCoordinates(translatedTargetPosition);
                fromX = position.getX();
                toX = centeredTargetPosition.getX();
                fromY = position.getY();
                toY = centeredTargetPosition.getY();
            }
            else {
                targetScale = minimumScale;
                TouchPoint translatedPosition = mapTouchCoordinateToMatrix(eventPoint, scale);
                TouchPoint centeredTargetPosition = centerCoordinates(translatedPosition);
                fromX = centeredTargetPosition.getX();
                toX = 0;
                fromY = centeredTargetPosition.getY();
                toY = 0;
            }

            gestureAnimator.animateDoubleTap(fromX, toX, fromY, toY, scale, targetScale);
            return true;
        }

        private TouchPoint centerCoordinates(TouchPoint coordinates) {
            float x = coordinates.getX() + (imageBounds.right / 2);
            float y = coordinates.getY() + (imageBounds.bottom / 2);
            return new TouchPoint(x, y);
        }
    };

    public TouchManager(final Image imageView, final CropViewConfig cropViewConfig) {
        this.imageView = imageView;
        scaleGestureDetector = new ScaleGestureDetector(imageView.getContext(), scaleGestureListener);
        gestureDetector = new GestureDetector(imageView.getContext(), gestureListener);
        scaleGestureDetector.setQuickScaleEnabled(true);
        this.cropViewConfig = cropViewConfig;
        minimumScale = cropViewConfig.getMinScale();
        maximumScale = cropViewConfig.getMaxScale();
    }

    public void onEvent(TouchEvent event) {
        scaleGestureDetector.onTouchEvent(event);
        gestureDetector.onTouchEvent(event);
        if (isUpAction(event.getAction())) {
            ensureInsideViewport();
        }
    }

    public void applyPositioningAndScale(Matrix matrix) {


        matrix.postTranslate(-bitmapWidth / 2.0f, -bitmapHeight / 2.0f);
        matrix.postScale(scale, scale);
        matrix.postTranslate(position.getX(), position.getY());
    }

    public void resetFor(int bitmapWidth, int bitmapHeight, int availableWidth, int availableHeight) {
        aspectRatio = cropViewConfig.getViewportRatio();
        imageBounds = new Rect(0, 0, availableWidth / 2, availableHeight / 2);
        setViewport(bitmapWidth, bitmapHeight, availableWidth, availableHeight);

        this.bitmapWidth = bitmapWidth;
        this.bitmapHeight = bitmapHeight;
        if (bitmapWidth > 0 && bitmapHeight > 0) {
            setMinimumScale();
            setLimits();
            resetPosition();
            ensureInsideViewport();
        }
    }

    public int getViewportWidth() {
        return viewportWidth;
    }

    public int getViewportHeight() {
        return viewportHeight;
    }

    public float getAspectRatio() {
        return aspectRatio;
    }

    public void  setAspectRatio(float ratio) {
        aspectRatio = ratio;
        cropViewConfig.setViewportRatio(ratio);
    }

    private TouchPoint mapTouchCoordinateToMatrix(TouchPoint coordinate, float targetScale) {
        float width = bitmapWidth * targetScale;
        float height = bitmapHeight * targetScale;

        float x0 = width / 2;
        float y0 = height / 2;

        float newX = coordinate.getX() * targetScale;
        newX = -(newX - x0);

        float newY = coordinate.getY() * targetScale;
        if(newY > y0) {
            newY = -(newY - y0);
        }
        else {
            newY = y0 - newY;
        }

        return new TouchPoint(newX, newY);
    }

    private void ensureInsideViewport() {
        if (imageBounds == null) {
            return;
        }

        float newY = position.getY();
        int bottom = imageBounds.bottom;


        if (bottom - newY >= verticalLimit) {
            newY = bottom - verticalLimit;
        } else if (newY - bottom >= verticalLimit) {
            newY = bottom + verticalLimit;
        }

        float newX = position.getX();
        int right = imageBounds.right;
        if (newX <= right - horizontalLimit) {
            newX = right - horizontalLimit;
        } else if (newX > right + horizontalLimit) {
            newX = right + horizontalLimit;
        }

        position.set(newX, newY);
    }

    private void setViewport(int bitmapWidth, int bitmapHeight, int availableWidth, int availableHeight) {
        final float imageAspect = (float) bitmapWidth / bitmapHeight;
        final float viewAspect = (float) availableWidth / availableHeight;

        float ratio = cropViewConfig.getViewportRatio();
        if (Float.compare(0f, ratio) == 0) {
            // viewport ratio of 0 means match native ratio of bitmap
            ratio = imageAspect;
        }

        if (ratio > viewAspect) {
            // viewport is wider than view
            viewportWidth = availableWidth - cropViewConfig.getViewportOverlayPadding() * 2;
            viewportHeight = (int) (viewportWidth * (1 / ratio));
        } else {
            // viewport is taller than view
            viewportHeight = availableHeight - cropViewConfig.getViewportOverlayPadding() * 2;
            viewportWidth = (int) (viewportHeight * ratio);
        }
    }

    private void  setLimits() {
        horizontalLimit = computeLimit((int) (bitmapWidth * scale), viewportWidth);
        verticalLimit = computeLimit((int) (bitmapHeight * scale), viewportHeight);
    }

    private void resetPosition() {
        position.set(imageBounds.right, imageBounds.bottom);
    }

    private void setMinimumScale() {
        final float fw = (float) viewportWidth / bitmapWidth;
        final float fh = (float) viewportHeight / bitmapHeight;
        minimumScale = Math.max(fw, fh);
        scale = Math.max(scale, minimumScale);
    }

    private float calculateScale(float newScaleDelta) {
        return Math.max(minimumScale, Math.min(scale * newScaleDelta, maximumScale));
    }

    private static int computeLimit(int bitmapSize, int viewportSize) {
        return (bitmapSize - viewportSize) / 2;
    }

    private static boolean isUpAction(int actionMasked) {
        return actionMasked == TouchEvent.OTHER_POINT_UP || actionMasked == TouchEvent.PRIMARY_POINT_UP;
    }

    private static class GestureAnimator {
        @Retention(RetentionPolicy.SOURCE)
        public @interface AnimationType {}
        public static final int ANIMATION_X = 0;
        public static final int ANIMATION_Y = 1;
        public static final int ANIMATION_SCALE = 2;

        interface OnAnimationUpdateListener {
            void onAnimationUpdate(@AnimationType int animationType, float animationValue);
            void onAnimationFinished();
        }

        private AnimatorValue xAnimator;
        private AnimatorValue yAnimator;
        private AnimatorValue scaleAnimator;

        private AnimatorGroup animator;

        private final OnAnimationUpdateListener listener;

        public GestureAnimator(OnAnimationUpdateListener listener) {
            this.listener = listener;
        }

        final AnimatorValue.ValueUpdateListener updateListener = new AnimatorValue.ValueUpdateListener() {
            @Override
            public void onUpdate(AnimatorValue animatorValue, float v) {
                if(animatorValue == xAnimator) {
                    listener.onAnimationUpdate(ANIMATION_X, v);
                }
                else if(animatorValue == yAnimator) {
                    listener.onAnimationUpdate(ANIMATION_Y, v);
                }
                else if(animatorValue == scaleAnimator) {
                    listener.onAnimationUpdate(ANIMATION_SCALE, v);
                }
            }
        };
        private final Animator.StateChangedListener animatorListener = new Animator.StateChangedListener() {
            @Override
            public void onStart(Animator animator) {

            }

            @Override
            public void onStop(Animator animator) {

            }

            @Override
            public void onCancel(Animator animator) {

            }

            @Override
            public void onEnd(Animator animator1) {
                if(xAnimator != null) xAnimator.setValueUpdateListener(null);
                if(yAnimator != null) yAnimator.setValueUpdateListener(null);
                if(scaleAnimator != null) scaleAnimator.setValueUpdateListener(null);
                animator.clear();
                listener.onAnimationFinished();
            }

            @Override
            public void onPause(Animator animator) {

            }

            @Override
            public void onResume(Animator animator) {

            }

        };

        public void animateTranslation(float fromX, float toX, float fromY, float toY) {
            if(animator != null) {
                animator.cancel();
            }

//            xAnimator = ValueAnimator.ofFloat(fromX, toX);
//            yAnimator = ValueAnimator.ofFloat(fromY, toY);
            xAnimator = new AnimatorValue();
            yAnimator = new AnimatorValue();
            scaleAnimator = null;

            xAnimator.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
                @Override
                public void onUpdate(AnimatorValue animatorValue, float v) {
                    listener.onAnimationUpdate(ANIMATION_X, getAnimatedValue(v,fromX,toX));
                }
            });
            yAnimator.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
                @Override
                public void onUpdate(AnimatorValue animatorValue, float v) {
                    listener.onAnimationUpdate(ANIMATION_Y, getAnimatedValue(v,fromY,toY));
                }
            });

            animate(new DecelerateInterpolator(), 250, xAnimator, yAnimator);
        }

        public void animateDoubleTap(float fromX, float toX, float fromY, float toY, float fromScale, float toScale) {
            if(animator != null) {
                animator.cancel();
            }

            xAnimator = new AnimatorValue();
            yAnimator = new AnimatorValue();
            scaleAnimator = new AnimatorValue();

            xAnimator.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
                @Override
                public void onUpdate(AnimatorValue animatorValue, float v) {
                    listener.onAnimationUpdate(ANIMATION_X, getAnimatedValue(v,fromX,toX));
                }
            });
            yAnimator.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
                @Override
                public void onUpdate(AnimatorValue animatorValue, float v) {
                    listener.onAnimationUpdate(ANIMATION_Y, getAnimatedValue(v,fromY,toY));
                }
            });
            scaleAnimator.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
                @Override
                public void onUpdate(AnimatorValue animatorValue, float v) {
                    listener.onAnimationUpdate(ANIMATION_SCALE, getAnimatedValue(v,fromScale,toScale));
                }
            });

            animate(new AccelerateDecelerateInterpolator(), 500, scaleAnimator, xAnimator, yAnimator);
        }

        private void animate(Animator.TimelineCurve interpolator, long duration, AnimatorValue first, AnimatorValue... animators) {
            animator = new AnimatorGroup();
            animator.setDuration(duration);
            animator.setCurve(interpolator);
            animator.setStateChangedListener(animatorListener);
            AnimatorGroup.Builder builder = animator.build().addAnimators(first);
            for(AnimatorValue valueAnimator : animators) {
                builder.addAnimators(valueAnimator);
            }
            animator.start();
        }
    }

    public static float getAnimatedValue(float fraction, float... values){
        if(values == null || values.length == 0){
            return 0;
        }
        if(values.length == 1){
            return values[0] * fraction;
        }else{
            if(fraction == 1){
                return values[values.length-1];
            }
            float oneFraction = 1f / (values.length - 1);
            float offFraction = 0;
            for (int i = 0; i < values.length - 1; i++) {
                if (offFraction + oneFraction >= fraction) {
                    return values[i] + (fraction - offFraction) * (values.length - 1) * (values[i + 1] - values[i]);
                }
                offFraction += oneFraction;
            }
        }
        return 0;
    }
}
