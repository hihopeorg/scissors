/*
 * Copyright (C) 2015 Lyft, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lyft.ohos.scissors2;






import com.lyft.ohos.scissors2.picasso.Transformation;
import ohos.agp.utils.Rect;
import ohos.media.image.PixelMap;
import ohos.media.image.common.Size;

class PicassoFillViewportTransformation implements Transformation {

    private final int viewportWidth;
    private final int viewportHeight;

    public PicassoFillViewportTransformation(int viewportWidth, int viewportHeight) {
        this.viewportWidth = viewportWidth;
        this.viewportHeight = viewportHeight;
    }

    @Override
    public PixelMap transform(PixelMap source) {
        int sourceWidth = source.getImageInfo().size.width;
        int sourceHeight = source.getImageInfo().size.height;

        Rect target = CropViewExtensions.computeTargetSize(sourceWidth, sourceHeight, viewportWidth, viewportHeight);

        PixelMap.InitializationOptions initializationOptions = new PixelMap.InitializationOptions();
        initializationOptions.size = new Size(target.getWidth(),target.getHeight());

        final PixelMap result = PixelMap.create(source, initializationOptions);

        if (result != source) {
            source.release();
        }

        return result;
    }

    @Override
    public String key() {
        return viewportWidth + "x" + viewportHeight;
    }

    public static Transformation createUsing(int viewportWidth, int viewportHeight) {
        return new PicassoFillViewportTransformation(viewportWidth, viewportHeight);
    }
}
