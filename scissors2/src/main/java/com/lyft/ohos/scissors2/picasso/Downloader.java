/*
 * Copyright (C) 2013 Square, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lyft.ohos.scissors2.picasso;

import okhttp3.Response;
import java.io.IOException;

/**
 * The interface Downloader.
 */
interface Downloader {
    /**
     * Download the specified image {@code url} from the internet.
     *
     * @param request data
     * @return the response
     * @throws IOException if the requested URL cannot successfully be loaded.
     */
    Response load(okhttp3.Request request) throws IOException;

    /**
     * Allows to perform a clean up for this {@link Downloader} including closing the disk cache and
     * other resources.
     */
    void shutdown();
}
