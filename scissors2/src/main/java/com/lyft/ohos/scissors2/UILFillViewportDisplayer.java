package com.lyft.ohos.scissors2;


import com.lyft.ohos.scissors2.util.BitmapDisplayer;
import com.lyft.ohos.scissors2.util.ImageAware;
import com.lyft.ohos.scissors2.util.LoadedFrom;
import ohos.agp.utils.Rect;
import ohos.media.image.PixelMap;
import ohos.media.image.common.Size;

class UILFillViewportDisplayer implements BitmapDisplayer {
    private final int viewportWidth;
    private final int viewportHeight;

    public UILFillViewportDisplayer(int viewportWidth, int viewportHeight) {
        this.viewportWidth = viewportWidth;
        this.viewportHeight = viewportHeight;
    }

    public static BitmapDisplayer createUsing(int viewportWidth, int viewportHeight) {
        return new UILFillViewportDisplayer(viewportWidth, viewportHeight);
    }

    @Override
    public void display(PixelMap source, ImageAware imageAware, LoadedFrom loadedFrom) {
        int sourceWidth = source.getImageInfo().size.width;
        int sourceHeight = source.getImageInfo().size.height;

        Rect target = CropViewExtensions.computeTargetSize(sourceWidth, sourceHeight, viewportWidth, viewportHeight);

        PixelMap.InitializationOptions initializationOptions = new PixelMap.InitializationOptions();
        initializationOptions.size = new Size(target.getWidth(), target.getHeight());

        final PixelMap result = PixelMap.create(source, initializationOptions);

        if (result != source) {
            source.release();
        }

        imageAware.setImageBitmap(result);
    }
}
