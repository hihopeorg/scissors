/*
 * Copyright (C) 2013 Square, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lyft.ohos.scissors2.picasso;


import ohos.app.Context;
import ohos.global.resource.ResourceManager;
import ohos.utils.net.Uri;

import java.io.IOException;

import okio.Okio;
import okio.Source;

import java.nio.file.Path;

import static com.lyft.ohos.scissors2.picasso.Picasso.LoadedFrom.DISK;


class AssetRequestHandler extends RequestHandler {
    protected static final String HARMONY_ASSET = "HARMONY_ASSET";
   private static final int ASSET_PREFIX_LENGTH=22; // Asset is not supported in openharmony as of now."file" + ":///" + HARMONY_ASSET + "/").length()
    private final Context context;
    private final Object lock = new Object();
    private ResourceManager assetManager;

    AssetRequestHandler(Context context) {
        this.context = context;
    }

    @Override
    public boolean canHandleRequest(Request data) {
        Uri uri = data.uri;
        return ("file".equals(uri.getScheme())
            && !uri.getDecodedPathList().isEmpty() && HARMONY_ASSET.equals(uri.getDecodedPathList().get(0)));
    }

    @Override
    public Result load(Request request, int networkPolicy) throws IOException {
        if (assetManager == null) {
            synchronized (lock) {
                if (assetManager == null) {
                    assetManager = context.getResourceManager();
                }
            }
        }
        assert assetManager != null;
        Source source = Okio.source((Path) assetManager.getRawFileEntry(getFilePath(request)));// need to check
        return new Result(source, DISK);
    }

    static String getFilePath(Request request) {
        return request.uri.toString().substring(ASSET_PREFIX_LENGTH);
    }
}
