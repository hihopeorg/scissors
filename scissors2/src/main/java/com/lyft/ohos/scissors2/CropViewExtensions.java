/*
 * Copyright (C) 2015 Lyft, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lyft.ohos.scissors2;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.ability.fraction.Fraction;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.utils.Rect;
import ohos.event.intentagent.IntentAgentConstant;
import ohos.utils.IntentConstants;


class CropViewExtensions {

    static void pickUsing(Ability activity, int requestCode) {
        activity.startAbilityForResult(
                createChooserIntent(),
                requestCode);
    }

    static void pickUsing(Fraction fragment, int requestCode) {
        fragment.getFractionAbility().startAbilityForResult(
                createChooserIntent(),
                requestCode);
    }

    private static Intent createChooserIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        Operation operation = new Intent.OperationBuilder().withAction(IntentConstants.ACTION_CHOOSE).build();
        //intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.setOperation(operation);
        return intent;
    }

    final static boolean HAS_PICASSO = canHasClass("com.lyft.ohos.scissors2.picasso.Picasso");
    final static boolean HAS_GLIDE = canHasClass("com.bumptech.glide.Glide");
    final static boolean HAS_UIL = canHasClass("com.nostra13.universalimageloader.core.ImageLoader");

    static BitmapLoader resolveBitmapLoader(CropView cropView, CropView.Extensions.LoaderType loaderType) {
        switch (loaderType) {
            case PICASSO:
                return PicassoBitmapLoader.createUsing(cropView);
            case GLIDE:
                return GlideBitmapLoader.createUsing(cropView);
            case UIL:
                //return UILBitmapLoader.createUsing(cropView);
            case CLASS_LOOKUP:
                break;
            default:
                throw new IllegalStateException("Unsupported type of loader = " + loaderType);
        }

        if (HAS_PICASSO) {
            return PicassoBitmapLoader.createUsing(cropView);
        }
        if (HAS_GLIDE) {
            return GlideBitmapLoader.createUsing(cropView);
        }
        if (HAS_UIL) {
            //return UILBitmapLoader.createUsing(cropView);
        }
        throw new IllegalStateException("You must provide a BitmapLoader.");
    }

    static boolean canHasClass(String className) {
        try {
            Class.forName(className);
            return true;
        } catch (ClassNotFoundException e) {
        }
        return false;
    }

    static Rect computeTargetSize(int sourceWidth, int sourceHeight, int viewportWidth, int viewportHeight) {

        if (sourceWidth == viewportWidth && sourceHeight == viewportHeight) {
            return new Rect(0, 0, viewportWidth, viewportHeight); // Fail fast for when source matches exactly on viewport
        }

        float scale;
        if (sourceWidth * viewportHeight > viewportWidth * sourceHeight) {
            scale = (float) viewportHeight / (float) sourceHeight;
        } else {
            scale = (float) viewportWidth / (float) sourceWidth;
        }
        final int recommendedWidth = (int) ((sourceWidth * scale) + 0.5f);
        final int recommendedHeight = (int) ((sourceHeight * scale) + 0.5f);
        return new Rect(0, 0, recommendedWidth, recommendedHeight);
    }
}
