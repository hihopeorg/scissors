package com.lyft.ohos.scissors2;


import ohos.agp.components.Component;

import static com.lyft.ohos.scissors2.CropViewExtensions.resolveBitmapLoader;

public class LoadRequest {

    private final CropView cropView;
    private BitmapLoader bitmapLoader;
    private CropView.Extensions.LoaderType loaderType = CropView.Extensions.LoaderType.CLASS_LOOKUP;

    LoadRequest(CropView cropView) {
        Utils.checkNotNull(cropView, "cropView == null");
        this.cropView = cropView;
    }

    public LoadRequest using(BitmapLoader bitmapLoader) {
        this.bitmapLoader = bitmapLoader;
        return this;
    }


    public LoadRequest using(CropView.Extensions.LoaderType loaderType) {
        this.loaderType = loaderType;
        return this;
    }


    public void load(Object model) {

        if (cropView.getWidth() == 0 && cropView.getHeight() == 0) {
            // Defer load until layout pass
            deferLoad(model);
            return;
        }
        performLoad(model);
    }

    void performLoad(Object model) {
        if (bitmapLoader == null) {
            bitmapLoader = resolveBitmapLoader(cropView, loaderType);
        }
        bitmapLoader.load(model, cropView);
    }

    void deferLoad(final Object model) {
//        if (!cropView.getComponentTreeObserver().isAlive()) {
//            return;
//        }
        cropView.setLayoutRefreshedListener(
                new Component.LayoutRefreshedListener() {
                    @Override
                    public void onRefreshed(Component component) {
                        if(cropView.getLayoutRefreshedListener()!=null) {
                            cropView.setLayoutRefreshedListener(null);
                        }
                        performLoad(model);
                    }
                }
        );
    }
}
