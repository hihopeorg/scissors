/*
 * Copyright (C) 2013 Square, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lyft.ohos.scissors2.picasso;


import ohos.aafwk.ability.DataAbilityHelper;
import ohos.aafwk.ability.DataAbilityRemoteException;
import ohos.app.Context;
import ohos.data.resultset.ResultSet;
import ohos.utils.net.Uri;


class MediaStoreRequestHandler extends ContentStreamRequestHandler {
    private static final String[] CONTENT_ORIENTATION = new String[] {
       // AVStorage.Images.ImageColumns.ORIENTATION
        "orientation"
    };

    MediaStoreRequestHandler(Context context) {
        super(context);
    }

    @Override public boolean canHandleRequest(Request data) {
        final Uri uri = data.uri;
        return ("content".equals(uri.getScheme())
            && "media".equals(uri.getDecodedAuthority()));
    }

    static PicassoKind getPicassoKind(int targetWidth, int targetHeight) {
        if (targetWidth <= PicassoKind.MICRO.width && targetHeight <= PicassoKind.MICRO.height) {
            return PicassoKind.MICRO;
        } else if (targetWidth <= PicassoKind.MINI.width && targetHeight <= PicassoKind.MINI.height) {
            return PicassoKind.MINI;
        }
        return PicassoKind.FULL;
    }

    static int getExifOrientation(DataAbilityHelper contentResolver, Uri uri) {
        ResultSet cursor = null;
        try {
            cursor = contentResolver.query(uri, CONTENT_ORIENTATION, null);
            if (cursor == null || !cursor.goToFirstRow()) {
                return 0;
            }
                return cursor.getInt(0);
        } catch (RuntimeException | DataAbilityRemoteException ignored) {
            // If the orientation column doesn't exist, assume no rotation.
                return 0;
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    enum PicassoKind {
        MICRO(3, 96, 96),
        MINI(1, 512, 384),
        FULL(2, -1, -1);

        final int harmonyKind;
        final int width;
        final int height;

        PicassoKind(int harmonyKind, int width, int height) {
            this.harmonyKind = harmonyKind;
            this.width = width;
            this.height = height;
        }
    }
}
