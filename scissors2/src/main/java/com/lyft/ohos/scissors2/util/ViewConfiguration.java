/*
 * Copyright (C) 2006 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lyft.ohos.scissors2.util;

public class ViewConfiguration {

    private static final int DEFAULT_LONG_PRESS_TIMEOUT = 500;

    private static final int TAP_TIMEOUT = 100;

    private static final int DOUBLE_TAP_TIMEOUT = 300;

    private static final int DOUBLE_TAP_MIN_TIME = 40;

    private static final int TOUCH_SLOP = 8;

    private static final int DOUBLE_TAP_SLOP = 100;

    private static final int MINIMUM_FLING_VELOCITY = 50;

    private static final int MAXIMUM_FLING_VELOCITY = 8000;

    public static int getLongPressTimeout() {
        return DEFAULT_LONG_PRESS_TIMEOUT;
    }

    public static int getTapTimeout() {
        return TAP_TIMEOUT;
    }

    public static int getDoubleTapTimeout() {
        return DOUBLE_TAP_TIMEOUT;
    }

    public static int getDoubleTapMinTime() {
        return DOUBLE_TAP_MIN_TIME;
    }

    public static int getTouchSlop() {
        return TOUCH_SLOP;
    }

    public static int getDoubleTapSlop() {
        return DOUBLE_TAP_SLOP;
    }

    public static int getMinimumFlingVelocity() {
        return MINIMUM_FLING_VELOCITY;
    }

    public static int getMaximumFlingVelocity() {
        return MAXIMUM_FLING_VELOCITY;
    }

    public static int getScaledTouchSlop(){
        return TOUCH_SLOP;
    }
}
