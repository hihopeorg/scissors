/*
 * Copyright (C) 2013 Square, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lyft.ohos.scissors2.picasso;


import ohos.aafwk.ability.DataAbilityHelper;
import ohos.app.Context;
import ohos.app.IAbilityManager;
import ohos.data.usage.StatVfs;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.eventhandler.InnerEvent;
import ohos.global.resource.ResourceManager;
import ohos.media.image.PixelMap;
import ohos.os.ProcessManager;
import ohos.sysappcomponents.settings.SystemSettings;
import okio.BufferedSource;
import okio.ByteString;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.ThreadFactory;

import static ohos.eventhandler.InnerEvent.get;
import static java.lang.String.format;

class Utils {
    private static final String TAG = "Utils";
    static final String THREAD_PREFIX = "Picasso-";
    static final String THREAD_IDLE_NAME = THREAD_PREFIX + "Idle";
    private static final String PICASSO_CACHE = "picasso-cache";
    private static final int KEY_PADDING = 50; // Determined by exact science.
    private static final int MIN_DISK_CACHE_SIZE = 5 * 1024 * 1024; // 5MB
    private static final int MAX_DISK_CACHE_SIZE = 50 * 1024 * 1024; // 50MB
    private static final int DIV_DISK_CACHE_SIZE = 50;
    static final int THREAD_LEAK_CLEANING_MS = 1000;
    static final char KEY_SEPARATOR = '\n';

    /** Thread confined to main thread for key creation. */
    static final StringBuilder MAIN_THREAD_KEY_BUILDER = new StringBuilder();

    /** Logging */
    static final String OWNER_MAIN = "Main";
    static final String OWNER_DISPATCHER = "Dispatcher";
    static final String OWNER_HUNTER = "Hunter";
    static final String VERB_CREATED = "created";
    static final String VERB_CHANGED = "changed";
    static final String VERB_IGNORED = "ignored";
    static final String VERB_ENQUEUED = "enqueued";
    static final String VERB_CANCELED = "canceled";
    static final String VERB_BATCHED = "batched";
    static final String VERB_RETRYING = "retrying";
    static final String VERB_EXECUTING = "executing";
    static final String VERB_DECODED = "decoded";
    static final String VERB_TRANSFORMED = "transformed";
    static final String VERB_JOINED = "joined";
    static final String VERB_REMOVED = "removed";
    static final String VERB_DELIVERED = "delivered";
    static final String VERB_REPLAYING = "replaying";
    static final String VERB_COMPLETED = "completed";
    static final String VERB_ERRORED = "errored";
    static final String VERB_PAUSED = "paused";
    static final String VERB_RESUMED = "resumed";

    private static final ByteString WEBP_FILE_HEADER_RIFF = ByteString.encodeUtf8("RIFF");
    private static final ByteString WEBP_FILE_HEADER_WEBP = ByteString.encodeUtf8("WEBP");

    private Utils() {
        // No instances.
    }



    static int getBitmapBytes(PixelMap bitmap) {
        int result = (int) bitmap.getPixelBytesNumber();
        if (result < 0) {
            throw new IllegalStateException("Negative size: " + bitmap);
        }
        return result;
    }

    static void checkNotMain() {
        if (isMain()) {
            throw new IllegalStateException("Method call should not happen from the main thread.");
        }
    }

    static void checkMain() {
        if (!isMain()) {
            throw new IllegalStateException("Method call should happen from the main thread.");
        }
    }

    static <T> T checkNotNull(T value, String message) {
        if (value == null) {
            throw new NullPointerException(message);
        }
        return value;
    }

   static String getLogIdsForHunter(BitmapHunter hunter) {
        return getLogIdsForHunter(hunter, "");
    }

    static String getLogIdsForHunter(BitmapHunter hunter, String prefix) {
        StringBuilder builder = new StringBuilder(prefix);
        Action action = hunter.getAction();
        if (action != null) {
            builder.append(action.request.logId());
        }
        List<Action> actions = hunter.getActions();
        if (actions != null) {
            for (int i = 0, count = actions.size(); i < count; i++) {
                if (i > 0 || action != null) builder.append(", ");
                builder.append(actions.get(i).request.logId());
            }
        }
        return builder.toString();
    }

    static boolean isMain() {
        return EventRunner.getMainEventRunner().isCurrentRunnerThread();
    }

    static void log(String owner, String verb, String logId) {
        log(owner, verb, logId, "");
    }

    static void log(String owner, String verb, String logId, String extras) {
         LogUtil.debug(TAG, format("%1$-11s %2$-12s %3$s %4$s", owner, verb, logId, extras));
    }

    static String createKey(Request data) {
        String result = createKey(data, MAIN_THREAD_KEY_BUILDER);
        MAIN_THREAD_KEY_BUILDER.setLength(0);
        return result;
    }

    static String createKey(Request data, StringBuilder builder) {
        if (data.stableKey != null) {
            builder.ensureCapacity(data.stableKey.length() + KEY_PADDING);
            builder.append(data.stableKey);
        } else if (data.uri != null) {
            String path = data.uri.toString();
            builder.ensureCapacity(path.length() + KEY_PADDING);
            builder.append(path);
        } else {
            builder.ensureCapacity(KEY_PADDING);
            builder.append(data.resourceId);
        }
        builder.append(KEY_SEPARATOR);

        if (data.rotationDegrees != 0) {
            builder.append("rotation:").append(data.rotationDegrees);
            if (data.hasRotationPivot) {
                builder.append('@').append(data.rotationPivotX).append('x').append(data.rotationPivotY);
            }
            builder.append(KEY_SEPARATOR);
        }
        if (data.hasSize()) {
            builder.append("resize:").append(data.targetWidth).append('x').append(data.targetHeight);
            builder.append(KEY_SEPARATOR);
        }
        if (data.centerCrop) {
            builder.append("centerCrop:").append(data.centerCropGravity).append(KEY_SEPARATOR);
        } else if (data.centerInside) {
            builder.append("centerInside").append(KEY_SEPARATOR);
        }

        if (data.transformations != null) {
            //noinspection ForLoopReplaceableByForEach
            for (int i = 0, count = data.transformations.size(); i < count; i++) {
                builder.append(data.transformations.get(i).key());
                builder.append(KEY_SEPARATOR);
            }
        }

        return builder.toString();
    }

    static File createDefaultCacheDir(Context context) {
        File cache = new File(context.getApplicationContext().getCacheDir(), PICASSO_CACHE);
        if (!cache.exists()) {
            //noinspection ResultOfMethodCallIgnored
            cache.mkdirs();
        }
        return cache;
    }

    static long calculateDiskCacheSize(File dir) {
        StatVfs statVfs = null;
        try {
            statVfs = new StatVfs(dir.getCanonicalPath());
        } catch (IOException e) {
            e.printStackTrace();
        }
        // Target 2% of the total space.
        long size = statVfs.getSpace() / DIV_DISK_CACHE_SIZE;
        // Bound inside min/max size for disk cache.
        return Math.max(Math.min(size, MAX_DISK_CACHE_SIZE), MIN_DISK_CACHE_SIZE);
    }


    static int calculateMemoryCacheSize(Context context) {
        IAbilityManager am = getService(context, "ability" );
        // boolean largeHeap = (context.getApplicationInfo().flags & FLAG_LARGE_HEAP) != 0;
        boolean largeHeap = context.getApplicationInfo().getFlags()!=0;
        int memoryClass = largeHeap ? am.getAppLargeMemory():am.getAppMemory();
        // Target ~15% of the available heap.
        return (int) (1024L * 1024L * memoryClass / 7);
    }

    static boolean isAirplaneModeOn(Context context) {
        DataAbilityHelper dataAbilityHelper = DataAbilityHelper.creator(context);   //TODO Need to check
        String strAirplaneMode = SystemSettings.getValue(dataAbilityHelper, SystemSettings.General.AIRPLANE_MODE_STATUS);
        boolean isEnabled = false;
        if (strAirplaneMode != null && !strAirplaneMode.equals("")) {
            isEnabled = Integer.valueOf(strAirplaneMode) == 1 ? true : false;
        }
        return isEnabled;
    }

    static <T> T getService(Context context, String service) {
        return (T) context.getAbilityManager();
    }

    static boolean hasPermission(Context context, String permission) {
        return context.verifyCallingOrSelfPermission(permission) == 0;
    }

    static ResourceManager getResources(Context context, Request data) throws FileNotFoundException {
        if (data.resourceId != 0 || data.uri == null) {
            return context.getResourceManager();
        }

        String pkg = data.uri.getDecodedAuthority();
        if (pkg == null) throw new FileNotFoundException("No package provided: " + data.uri);
        try {
            ResourceManager pm = context.getResourceManager();
            return pm;
        } catch (Exception e) {
            throw new FileNotFoundException("Unable to obtain resources for package: " + data.uri);
        }
    }

   /**
     * HandlerThread always keeps a stack local reference to the last message
     * that was sent to it. This method makes sure that stack local reference never stays there
     * for too long by sending new messages to it every second.
    *
    * @param looper the looper value
    */
   static void flushStackLocalLeaks(EventRunner looper) {
       EventHandler handler = new EventHandler(looper) {
           @Override
           protected void processEvent(InnerEvent event) {
               super.processEvent(event);
               sendEvent(get(), THREAD_LEAK_CLEANING_MS);
           }
       };
       handler.sendEvent(get(), THREAD_LEAK_CLEANING_MS);
   }

    static class PicassoThreadFactory implements ThreadFactory {
        @SuppressWarnings("NullableProblems")
        public Thread newThread(Runnable r) {
            return new PicassoThread(r);
        }
    }

    private static class PicassoThread extends Thread {
        PicassoThread(Runnable r) {
            super(r);
        }

        @Override public void run() {
            ProcessManager.setThreadPriority(10);
            super.run();
        }
    }

    static boolean isWebPFile(BufferedSource source) throws IOException {
        return source.rangeEquals(0, WEBP_FILE_HEADER_RIFF)
            && source.rangeEquals(8, WEBP_FILE_HEADER_WEBP);
    }

    static int getResourceId(ResourceManager resources, Request data) throws FileNotFoundException {
        if (data.resourceId != 0 || data.uri == null) {
            return data.resourceId;
        }

        String pkg = data.uri.getDecodedAuthority();
        if (pkg == null) throw new FileNotFoundException("No package provided: " + data.uri);

        int id = 0;
        List<String> segments = data.uri.getDecodedPathList();
        if (segments == null || segments.isEmpty()) {
            throw new FileNotFoundException("No path segments: " + data.uri);
        } else if (segments.size() == 1) {
            try {
                id = Integer.parseInt(segments.get(0));
            } catch (NumberFormatException e) {
                throw new FileNotFoundException("Last path segment is not a resource ID: " + data.uri);
            }
        } else if (segments.size() == 2) {
            String type = segments.get(0);
            String name = segments.get(1);
        } else {
            throw new FileNotFoundException("More than two path segments: " + data.uri);
        }
        return id;
    }

}
