/*
 * Copyright (C) 2015 Lyft, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lyft.ohos.scissors2;


import ohos.aafwk.ability.Ability;
import ohos.aafwk.ability.fraction.Fraction;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.Image;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.PixelMapElement;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.render.Path;
import ohos.agp.render.PixelMapHolder;
import ohos.agp.render.Texture;
import ohos.agp.utils.Color;
import ohos.agp.utils.Matrix;
import ohos.agp.utils.Rect;
import ohos.agp.utils.RectFloat;
import ohos.app.Context;
import ohos.global.resource.NotExistException;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;
import ohos.media.image.common.PixelFormat;
import ohos.media.image.common.Size;
import ohos.multimodalinput.event.TouchEvent;
import ohos.utils.net.Uri;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;
import java.util.List;

public class CropView extends Image implements Component.DrawTask, Component.TouchEventListener, Component.LayoutRefreshedListener {

    private TouchManager touchManager;
    private CropViewConfig config;



    private List<Component> componentList = new ArrayList<>();
    private Paint viewportPaint = new Paint();
    private Paint bitmapPaint = new Paint();

    private PixelMap bitmap;
    private Matrix transform = new Matrix();
    private Extensions extensions;


    public List<Component> getComponentList() {
        return componentList;
    }

    public void setComponentList(List<Component> componentList) {
        this.componentList = componentList;
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        if (bitmap == null) {
            return;
        }
        int saveCount = canvas.save();
        drawBitmap(canvas);
        canvas.restoreToCount(saveCount);
        if (shape == Shape.RECTANGLE) {
            drawSquareOverlay(canvas);
        } else {
            drawOvalOverlay(canvas);
        }
    }

    @Override
    public boolean onTouchEvent(Component component, TouchEvent touchEvent) {

        if (!isEnabled()) {
            return true;
        }

        if(isClickStatus(touchEvent)&&isInComponent(touchEvent)){
            return false;
        }

        addTouchEvent(touchEvent);
        touchManager.onEvent(touchEvent);
        getContext().getUITaskDispatcher().asyncDispatch(this::invalidate);
        return true;
    }

    private boolean isInComponent(TouchEvent touchEvent){
        if(componentList.size()==0||touchEvent.getAction()!=TouchEvent.PRIMARY_POINT_DOWN) {
            return false;
        }

        for(Component c:componentList){
            int[] postion = c.getLocationOnScreen();
            float x = touchEvent.getPointerScreenPosition(touchEvent.getIndex()).getX();
            float y = touchEvent.getPointerScreenPosition(touchEvent.getIndex()).getY();
            RectFloat rect = new RectFloat(postion[0],postion[1],postion[0]+c.getWidth(),postion[1]+c.getHeight());
            if(rect.isInclude(x,y)){
                return true;
            }

        }
        return false;
    }

    private boolean isClickStatus(TouchEvent touchEvent){
        if(componentList.size()==0) {
            return false;
        }
        return touchEvent.getAction() == TouchEvent.PRIMARY_POINT_DOWN
                &&componentList.size()!=0
                &&componentList.get(0).getVisibility()==Component.VISIBLE;
    }

    private void addTouchEvent(TouchEvent touchEvent) {
        if(componentList.size()==0){
            return;
        }
        if (touchEvent.getPointerCount() > 1 || this.getImageBitmap() == null) {
            return;
        }

        switch (touchEvent.getAction()) {
            case TouchEvent.PRIMARY_POINT_DOWN:
            case TouchEvent.POINT_MOVE:
                for(Component c:componentList){
                    c.setVisibility(Component.INVISIBLE);
                }
                break;
            default:
                for(Component c:componentList){
                    c.setVisibility(Component.VISIBLE);
                }
                break;
        }
    }

    @Override
    public void onRefreshed(Component component) {
        resetTouchManager();
    }

    @Retention(RetentionPolicy.SOURCE)
    public @interface Shape {

        int RECTANGLE = 0;
        int OVAL = 1;
    }

    @Shape
    private int shape = Shape.RECTANGLE;
    private Path ovalPath;
    private RectFloat ovalRect;

    public CropView(Context context) {
        super(context);
        initCropView(context, null);
    }

    public CropView(Context context, AttrSet attrs) {
        super(context, attrs);

        initCropView(context, attrs);
    }

    void initCropView(Context context, AttrSet attrs) {
        addDrawTask(this::onDraw);
        setTouchEventListener(this::onTouchEvent);
        config = CropViewConfig.from(context, attrs);
        touchManager = new TouchManager(this, config);

        bitmapPaint.setFilterBitmap(true);
        setViewportOverlayColor(config.getViewportOverlayColor());
        shape = config.shape();

        // We need anti-aliased Paint to smooth the curved edges
        //viewportPaint.setFlags(viewportPaint.getFlags() | Paint.ANTI_ALIAS_FLAG);

        viewportPaint.setAntiAlias(true);
    }


    private void drawBitmap(Canvas canvas) {
        transform.reset();
        touchManager.applyPositioningAndScale(transform);
        //canvas.drawBitmap(bitmap, transform, bitmapPaint);
        canvas.concat(transform);
        canvas.drawPixelMapHolder(new PixelMapHolder(bitmap),0,0,bitmapPaint);


    }

    private void drawSquareOverlay(Canvas canvas) {
        final int viewportWidth = touchManager.getViewportWidth();
        final int viewportHeight = touchManager.getViewportHeight();
        final int left = (getWidth() - viewportWidth) / 2;
        final int top = (getHeight() - viewportHeight) / 2;
        canvas.drawRect(0, top, left, getHeight() - top, viewportPaint); // left
        canvas.drawRect(0, 0, getWidth(), top, viewportPaint); // top
        canvas.drawRect(getWidth() - left, top, getWidth(), getHeight() - top, viewportPaint); // right
        canvas.drawRect(0, getHeight() - top, getWidth(), getHeight(), viewportPaint); // bottom
    }

    private void drawOvalOverlay(Canvas canvas) {
        if (ovalRect == null) {
            ovalRect = new RectFloat();
        }
        if (ovalPath == null) {
            ovalPath = new Path();
        }

        final int viewportWidth = touchManager.getViewportWidth();
        final int viewportHeight = touchManager.getViewportHeight();
        final int left = (getWidth() - viewportWidth) / 2;
        final int top = (getHeight() - viewportHeight) / 2;
        final int right = getWidth() - left;
        final int bottom = getHeight() - top;
        ovalRect.left = left;
        ovalRect.top = top;
        ovalRect.right = right;
        ovalRect.bottom = bottom;

        // top left arc
        ovalPath.reset();
        ovalPath.moveTo(left, getHeight() / 2); // middle of the left side of the circle
        ovalPath.arcTo(ovalRect, 180, 90, false); // draw arc to top
        ovalPath.lineTo(left, top); // move to top-left corner
        ovalPath.lineTo(left, getHeight() / 2); // move back to origin
        ovalPath.close();
        canvas.drawPath(ovalPath, viewportPaint);

        // top right arc
        ovalPath.reset();
        ovalPath.moveTo(getWidth() / 2, top); // middle of the top side of the circle
        ovalPath.arcTo(ovalRect, 270, 90, false); // draw arc to the right
        ovalPath.lineTo(right, top); // move to top-right corner
        ovalPath.lineTo(getWidth() / 2, top); // move back to origin
        ovalPath.close();
        canvas.drawPath(ovalPath, viewportPaint);

        // bottom right arc
        ovalPath.reset();
        ovalPath.moveTo(right, getHeight() / 2); // middle of the right side of the circle
        ovalPath.arcTo(ovalRect, 0, 90, false); // draw arc to the bottom
        ovalPath.lineTo(right, bottom); // move to bottom-right corner
        ovalPath.lineTo(right, getHeight() / 2); // move back to origin
        ovalPath.close();
        canvas.drawPath(ovalPath, viewportPaint);

        // bottom left arc
        ovalPath.reset();
        ovalPath.moveTo(getWidth() / 2, bottom); // middle of the bottom side of the circle
        ovalPath.arcTo(ovalRect, 90, 90, false); // draw arc to the left
        ovalPath.lineTo(left, bottom); // move to bottom-left corner
        ovalPath.lineTo(getWidth() / 2, bottom); // move back to origin
        ovalPath.close();
        canvas.drawPath(ovalPath, viewportPaint);

        // Draw the square overlay as well
        drawSquareOverlay(canvas);
    }


    /**
     * Sets the color of the viewport overlay
     *
     * @param viewportOverlayColor The color to use for the viewport overlay
     */
    public void setViewportOverlayColor(int viewportOverlayColor) {
        viewportPaint.setColor(new Color(viewportOverlayColor));
        config.setViewportOverlayColor(viewportOverlayColor);
    }

    /**
     * Sets the padding for the viewport overlay
     *
     * @param viewportOverlayPadding The new padding of the viewport overlay
     */
    public void setViewportOverlayPadding(int viewportOverlayPadding) {
        config.setViewportOverlayPadding(viewportOverlayPadding);
        resetTouchManager();
        invalidate();
    }

    /**
     * Returns the native aspect ratio of the image.
     *
     * @return The native aspect ratio of the image.
     */
    public float getImageRatio() {
        PixelMap bitmap = getImageBitmap();
        return bitmap != null ? (float) bitmap.getImageInfo().size.width / (float) bitmap.getImageInfo().size.height : 0f;
    }

    /**
     * Returns the aspect ratio of the viewport and crop rect.
     *
     * @return The current viewport aspect ratio.
     */
    public float getViewportRatio() {
        return touchManager.getAspectRatio();
    }

    /**
     * Sets the aspect ratio of the viewport and crop rect.  Defaults to
     * the native aspect ratio if <code>ratio == 0</code>.
     *
     * @param ratio The new aspect ratio of the viewport.
     */
    public void setViewportRatio(float ratio) {
        if (Float.compare(ratio, 0) == 0) {
            ratio = getImageRatio();
        }
        touchManager.setAspectRatio(ratio);
        resetTouchManager();
        invalidate();
    }

    @Override
    public void setPixelMap(int resId) {
        final PixelMap bitmap = resId > 0
                ? getPixelMap(resId)
                : null;
        setPixelMap(bitmap);
    }

    @Override
    public void setImageElement(Element drawable) {
        final PixelMap bitmap;
        if (drawable instanceof PixelMapElement) {
            PixelMapElement bitmapDrawable = (PixelMapElement) drawable;
            bitmap = bitmapDrawable.getPixelMap();
        } else if (drawable != null) {
            bitmap = Utils.asBitmap(drawable, getWidth(), getHeight());
        } else {
            bitmap = null;
        }

        setPixelMap(bitmap);
    }

//    @Override
//    public void setImageURI(Uri uri) {
//        extensions().load(uri);
//    }

    @Override
    public void setPixelMap(PixelMap bitmap) {
        this.bitmap = bitmap;
        resetTouchManager();
        invalidate();
    }

    /**
     * @return Current working Bitmap or <code>null</code> if none has been set yet.
     */
    public PixelMap getImageBitmap() {
        return bitmap;
    }

    private void resetTouchManager() {
        final boolean invalidBitmap = bitmap == null;
        final int bitmapWidth = invalidBitmap ? 0 : bitmap.getImageInfo().size.width;
        final int bitmapHeight = invalidBitmap ? 0 : bitmap.getImageInfo().size.height;
        touchManager.resetFor(bitmapWidth, bitmapHeight, getWidth(), getHeight());
    }



    public PixelMap crop() {
        if (bitmap == null) {
            return null;
        }

        final PixelMap src = bitmap;
        final PixelFormat srcConfig = src.getImageInfo().pixelFormat;
        final PixelFormat config = srcConfig == null ? PixelFormat.ARGB_8888 : srcConfig;
        final int viewportHeight = touchManager.getViewportHeight();
        final int viewportWidth = touchManager.getViewportWidth();
        PixelMap.InitializationOptions initializationOptions = new PixelMap.InitializationOptions();
        initializationOptions.size = new Size(viewportWidth,viewportHeight);
        initializationOptions.pixelFormat  = config;
        final PixelMap dst = PixelMap.create(initializationOptions);
        Canvas canvas = new Canvas(new Texture(dst));
        final int left = (getRight() - viewportWidth) / 2;
        final int top = (getBottom() - viewportHeight) / 2;
        canvas.translate(-left, -top);
        drawBitmap(canvas);
        return dst;
    }

    /**
     * Obtain current viewport width.
     *
     * @return Current viewport width.
     * <p>Note: It might be 0 if layout pass has not been completed.</p>
     */
    public int getViewportWidth() {
        return touchManager.getViewportWidth();
    }

    /**
     * Obtain current viewport height.
     *
     * @return Current viewport height.
     * <p>Note: It might be 0 if layout pass has not been completed.</p>
     */
    public int getViewportHeight() {
        return touchManager.getViewportHeight();
    }

    /**
     * Offers common utility extensions.
     *
     * @return Extensions object used to perform chained calls.
     */
    public Extensions extensions() {
        if (extensions == null) {
            extensions = new Extensions(this);
        }
        return extensions;
    }

    /**
     * Get the transform matrix
     */
    public Matrix getTransformMatrix() {
        return transform;
    }

    /**
     * Optional extensions to perform common actions involving a {@link CropView}
     */
    public static class Extensions {

        private final CropView cropView;

        Extensions(CropView cropView) {
            this.cropView = cropView;
        }
        public void load(Object model) {
            new LoadRequest(cropView)
                    .load(model);
        }


        public LoadRequest using(BitmapLoader bitmapLoader) {
            return new LoadRequest(cropView).using(bitmapLoader);
        }

        public enum LoaderType {
            PICASSO,
            GLIDE,
            UIL,
            CLASS_LOOKUP
        }


        public LoadRequest using(LoaderType loaderType) {
            return new LoadRequest(cropView).using(loaderType);
        }

        /**
         * Perform an asynchronous crop request.
         *
         * @return {@link CropRequest} used to chain a configure cropping request, you must call either one of:
         * <ul>
         * <li>{@link CropRequest#into(File)}</li>
         * <li>{@link CropRequest#into(OutputStream, boolean)}</li>
         * </ul>
         */
        public CropRequest crop() {
            return new CropRequest(cropView);
        }


        public void pickUsing(Ability activity, int requestCode) {
            CropViewExtensions.pickUsing(activity, requestCode);
        }

        public void pickUsing(Fraction fragment, int requestCode) {
            CropViewExtensions.pickUsing(fragment, requestCode);
        }
    }


    public PixelMap getPixelMap(int imageRes){
        ImageSource.SourceOptions srcOpts = new ImageSource.SourceOptions();
        ImageSource imageSource = null;
        try {
            imageSource = ImageSource.create(getResourceManager()
                    .getResource(imageRes), srcOpts);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NotExistException e) {
            e.printStackTrace();
        }
        ImageSource.DecodingOptions decodingOpts = new ImageSource.DecodingOptions();
        return imageSource.createPixelmap(decodingOpts);
    }
}
