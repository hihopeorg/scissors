# scissors

**本项目是基于开源项目 scissors 进行ohos化的移植和开发的，可以通过项目标签以及github地址（https://github.com/lyft/scissors ）追踪到原项目版本**

#### 项目介绍

- 项目名称：scissors
- 所属系列：ohos的第三方组件适配移植
- 功能：提供图片裁剪
- 项目作者和维护人：hihope
- 联系方式：hihope@hoperun.com
- 调用差异：无
- 原项目Doc地址：https://github.com/lyft/scissors
- 原基线版本:v1.1.1
- 编程语言：Java 

#### 安装教程
方法1.
1. 下载源码编译三方库包scissors.har。
2. 启动 DevEco Studio，将编译的har包导入工程目录“entry->libs”下。
3. 在entry级别下的build.gradle文件中添加依赖，在dependences标签中增加对libs目录下har包的引用。
```
dependencies {
    implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
	……
}
```

方法2.
1. 在工程的build.gradle的allprojects中，添加HAR所在的Maven仓地址
```
repositories {
    maven {
        url 'http://106.15.92.248:8081/repository/Releases/' 
    }
}
```
2. 在应用模块的build.gradle的dependencies闭包中，添加如下代码:
```
dependencies {
    implementation 'com.lyft.ohos:scissors:1.0.0'
}
```
#### 效果演示
<img src="gif/演示效果.gif"/>

#### 使用说明

1.xml使用
```
<com.lyft.ohos.scissors2.CropView
        ohos:id="$+id:crop_view"
        ohos:height="match_parent"
        ohos:width="match_parent"/>
```
2.控件图片加载
```
Uri galleryPictureUri = resultData.getUri();
            cropView.extensions()
                    .load(galleryPictureUri);
```
3.设置裁剪区域宽高比例
```
cropView.setViewportRatio(getAnimatedValue(v,oldRatio, finalNewRatio));
```
4.得到裁剪图片
```
PixelMap resultPixelMap = cropView.crop();
```
#### 版本迭代

- v1.0.0

 实现功能：

1. 图片裁剪
2. 设置裁剪区域
3. 支持手势缩放图片

#### 版权和许可信息

```
Copyright (C) 2015 Lyft, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.


```


