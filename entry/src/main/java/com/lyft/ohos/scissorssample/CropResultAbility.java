package com.lyft.ohos.scissorssample;

import com.lyft.ohos.scissors2.picasso.MemoryPolicy;
import com.lyft.ohos.scissors2.picasso.Picasso;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.Image;
import ohos.media.image.PixelMap;

import java.io.File;

public class CropResultAbility extends Ability {
    private static final String EXTRA_FILE_PATH = "EXTRA_FILE_PATH";
    private static final String PIXELMAP = "PIXELMAP";
    Image resultView;
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_crop_result);
        resultView = (Image)findComponentById(ResourceTable.Id_result_image);
        resultView.setPixelMap(MainAbility.resultPixelMap);
    }



    public static void startUsing(Ability activity) {
        Intent intent = new Intent();
        Operation operation = new Intent.OperationBuilder()
                .withDeviceId("")
                .withBundleName("com.lyft.ohos.scissorssample")
                .withAbilityName("com.lyft.ohos.scissorssample.CropResultAbility")
                .build();
        intent.setOperation(operation);
        activity.startAbility(intent);
    }

}
