package com.lyft.ohos.scissorssample;

import com.lyft.ohos.scissors2.CropView;

import io.reactivex.rxjava3.core.Observable;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.Component;
import ohos.agp.components.Image;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.ElementScatter;
import ohos.agp.window.dialog.ToastDialog;
import ohos.global.resource.NotExistException;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;
import ohos.multimodalinput.event.TouchEvent;
import ohos.utils.net.Uri;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static io.reactivex.rxjava3.openharmony.schedulers.OpenHarmonySchedulers.mainThread;
import static io.reactivex.rxjava3.schedulers.Schedulers.io;

public class MainAbility extends Ability {
    public static PixelMap resultPixelMap;
    public CropView cropView;
    public Image crop_fab;
    public Image pick_mini_fab;
    public Image ratio_fab;
    public Image pick_fab;
    public Image result_image;
    public int selectedRatio = 0;
    public List<Component> componentList = new ArrayList<>();

    Animator.StateChangedListener animatorListener = new Animator.StateChangedListener() {
        @Override
        public void onStart(Animator animator) {
            crop_fab.setVisibility(Component.INVISIBLE);
            pick_mini_fab.setVisibility(Component.INVISIBLE);
            ratio_fab.setVisibility(Component.INVISIBLE);
        }

        @Override
        public void onStop(Animator animator) {

        }

        @Override
        public void onCancel(Animator animator) {

        }

        @Override
        public void onEnd(Animator animator) {
            crop_fab.setVisibility(Component.VISIBLE);
            pick_mini_fab.setVisibility(Component.VISIBLE);
            ratio_fab.setVisibility(Component.VISIBLE);
        }

        @Override
        public void onPause(Animator animator) {

        }

        @Override
        public void onResume(Animator animator) {

        }
    };

    private static final float[] ASPECT_RATIOS = { 0f, 1f, 6f/4f, 16f/9f };

    private static final String[] ASPECT_LABELS = { "\u00D8", "1:1", "6:4", "16:9" };
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        initComponent();
        initOnClick();
        getWindow().setStatusBarColor(ResourceTable.Color_color_1);
    }

    private void initComponent() {
        cropView = (CropView) findComponentById(ResourceTable.Id_crop_view);
        crop_fab = (Image) findComponentById(ResourceTable.Id_crop_fab);
        pick_mini_fab = (Image) findComponentById(ResourceTable.Id_pick_mini_fab);
        ratio_fab = (Image) findComponentById(ResourceTable.Id_ratio_fab);
        pick_fab = (Image) findComponentById(ResourceTable.Id_pick_fab);

        Element backElement = ElementScatter.getInstance(this).parse(ResourceTable.Graphic_background_button);
        crop_fab.setBackground(backElement);
        pick_mini_fab.setBackground(backElement);
        ratio_fab.setBackground(backElement);
        pick_fab.setBackground(backElement);
        result_image = (Image) findComponentById(ResourceTable.Id_result_image);
    }

    private void initOnClick() {
        crop_fab.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                  resultPixelMap = cropView.crop();
                  CropResultAbility.startUsing(MainAbility.this);
            }
        });

        pick_fab.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                cropView.extensions()
                        .pickUsing(MainAbility.this, RequestCodes.PICK_IMAGE_FROM_GALLERY);

            }
        });

        pick_mini_fab.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                cropView.extensions()
                        .pickUsing(MainAbility.this, RequestCodes.PICK_IMAGE_FROM_GALLERY);
            }
        });

        ratio_fab.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                final float oldRatio = cropView.getImageRatio();
                selectedRatio = (selectedRatio + 1) % ASPECT_RATIOS.length;

                // Since the animation needs to interpolate to the native
                // ratio, we need to get that instead of using 0
                float newRatio = ASPECT_RATIOS[selectedRatio];
                if (Float.compare(0, newRatio) == 0) {
                    newRatio = cropView.getImageRatio();
                }

                AnimatorValue viewportRatioAnimator = new AnimatorValue();
                float finalNewRatio = newRatio;
                viewportRatioAnimator.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
                    @Override
                    public void onUpdate(AnimatorValue animatorValue, float v) {
                        cropView.setViewportRatio(getAnimatedValue(v,oldRatio, finalNewRatio));
                    }
                });
                viewportRatioAnimator.setDuration(420);

                viewportRatioAnimator.setStateChangedListener(animatorListener);
                viewportRatioAnimator.start();

                new ToastDialog(getContext())
                        .setText(ASPECT_LABELS[selectedRatio])
                        .show();
            }
        });
        componentList.add(crop_fab);
        componentList.add(pick_mini_fab);
        componentList.add(ratio_fab);
        cropView.setComponentList(componentList);

    }


    @Override
    protected void onAbilityResult(int requestCode, int resultCode, Intent resultData) {
        super.onAbilityResult(requestCode, resultCode, resultData);
        if (requestCode == RequestCodes.PICK_IMAGE_FROM_GALLERY) {

            if(resultData != null){
                Uri galleryPictureUri = resultData.getUri();
                cropView.extensions()
                        .load(galleryPictureUri);
                updateButtons();
            }


        }
    }


    public void updateButtons() {
        crop_fab.setVisibility(Component.VISIBLE);
        pick_mini_fab.setVisibility(Component.VISIBLE);
        ratio_fab.setVisibility(Component.VISIBLE);
        pick_fab.setVisibility(Component.HIDE);
    }

    public PixelMap getPixelMap(int imageRes){
        ImageSource.SourceOptions srcOpts = new ImageSource.SourceOptions();
        ImageSource imageSource = null;
        try {
            imageSource = ImageSource.create(getResourceManager()
                    .getResource(imageRes), srcOpts);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NotExistException e) {
            e.printStackTrace();
        }
        ImageSource.DecodingOptions decodingOpts = new ImageSource.DecodingOptions();
        return imageSource.createPixelmap(decodingOpts);
    }

    public static float getAnimatedValue(float fraction, float... values){
        if(values == null || values.length == 0){
            return 0;
        }
        if(values.length == 1){
            return values[0] * fraction;
        }else{
            if(fraction == 1){
                return values[values.length-1];
            }
            float oneFraction = 1f / (values.length - 1);
            float offFraction = 0;
            for (int i = 0; i < values.length - 1; i++) {
                if (offFraction + oneFraction >= fraction) {
                    return values[i] + (fraction - offFraction) * (values.length - 1) * (values[i + 1] - values[i]);
                }
                offFraction += oneFraction;
            }
        }
        return 0;
    }
}
