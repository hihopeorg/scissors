package com.lyft.ohos.scissorssample.junit;

import com.lyft.ohos.scissors2.TouchPoint;
import org.junit.Test;

import static org.junit.Assert.*;

public class TouchPointTest {
    TouchPoint touchPoint = new TouchPoint();

    @Test
    public void getX() {
        assertEquals(0,touchPoint.getX(),0.0);
    }

    @Test
    public void getY() {
        assertEquals(0,touchPoint.getY(),0.0);
    }

    @Test
    public void getLength() {
        assertEquals(0,touchPoint.getLength(),0.0);
    }

    @Test
    public void set() {
        touchPoint.set(1,1);
        assertEquals(1,touchPoint.getX(),0.0);
    }
}