package com.lyft.ohos.scissorssample.junit;

import com.lyft.ohos.scissors2.CropView;
import com.lyft.ohos.scissors2.CropViewConfig;
import org.junit.Test;

import static org.junit.Assert.*;

public class CropViewConfigTest {
    CropViewConfig cropViewConfig = new CropViewConfig();

    @Test
    public void getViewportOverlayColor() {
        assertEquals(0xC8000000,cropViewConfig.getViewportOverlayColor());
    }

    @Test
    public void setViewportOverlayColor() {
        cropViewConfig.setViewportOverlayColor(0);
        assertEquals(0,cropViewConfig.getViewportOverlayColor());
    }

    @Test
    public void getViewportOverlayPadding() {
        assertEquals(0,cropViewConfig.getViewportOverlayPadding());
    }

    @Test
    public void setViewportOverlayPadding() {
        cropViewConfig.setViewportOverlayPadding(9);
        assertEquals(9,cropViewConfig.getViewportOverlayPadding());
    }

    @Test
    public void getViewportRatio() {
        assertEquals(1f,cropViewConfig.getViewportRatio(),0.0);
    }

    @Test
    public void setViewportRatio() {
        cropViewConfig.setViewportRatio(2f);
        assertEquals(2f,cropViewConfig.getViewportRatio(),0.0);
    }

    @Test
    public void getMaxScale() {
        assertEquals(10f,cropViewConfig.getMaxScale(),0.0);
    }

    @Test
    public void setMaxScale() {
        cropViewConfig.setMaxScale(5);
        assertEquals(5,cropViewConfig.getMaxScale(),0.0);
    }

    @Test
    public void getMinScale() {
        assertEquals(0f,cropViewConfig.getMinScale(),0.0);
    }

    @Test
    public void setMinScale() {
        cropViewConfig.setMinScale(1);
        assertEquals(1f,cropViewConfig.getMinScale(),0.0);
    }

    @Test
    public void shape() {
        assertEquals(CropView.Shape.RECTANGLE,cropViewConfig.shape());
    }

    @Test
    public void setShape() {
        cropViewConfig.setShape(CropView.Shape.OVAL);
        assertEquals(CropView.Shape.OVAL,cropViewConfig.shape());
    }
}