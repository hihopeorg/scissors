package com.lyft.ohos.scissorssample.junit;

import com.lyft.ohos.scissors2.util.ViewConfiguration;
import org.junit.Test;
import static org.junit.Assert.*;

public class ViewConfigurationTest {
    ViewConfiguration viewConfiguration = new ViewConfiguration();

    @Test
    public void getLongPressTimeout() {
        assertEquals(500,ViewConfiguration.getLongPressTimeout());
    }

    @Test
    public void getTapTimeout() {
        assertEquals(100,ViewConfiguration.getTapTimeout());
    }

    @Test
    public void getDoubleTapTimeout() {
        assertEquals(300,ViewConfiguration.getDoubleTapTimeout());
    }

    @Test
    public void getDoubleTapMinTime() {
        assertEquals(40,ViewConfiguration.getDoubleTapMinTime());
    }

    @Test
    public void getTouchSlop() {
        assertEquals(8,ViewConfiguration.getTouchSlop());
    }

    @Test
    public void getDoubleTapSlop() {
        assertEquals(100,ViewConfiguration.getDoubleTapSlop());
    }

    @Test
    public void getMinimumFlingVelocity() {
        assertEquals(50,ViewConfiguration.getMinimumFlingVelocity());
    }

    @Test
    public void getMaximumFlingVelocity() {
        assertEquals(8000,ViewConfiguration.getMaximumFlingVelocity());
    }

    @Test
    public void getScaledTouchSlop() {
        assertEquals(8,ViewConfiguration.getScaledTouchSlop());
    }
}