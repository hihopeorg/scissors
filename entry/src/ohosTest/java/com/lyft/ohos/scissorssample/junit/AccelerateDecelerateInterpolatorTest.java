package com.lyft.ohos.scissorssample.junit;

import com.lyft.ohos.scissors2.util.AccelerateDecelerateInterpolator;
import org.junit.Test;
import static org.junit.Assert.assertEquals;
public class AccelerateDecelerateInterpolatorTest {

    @Test
    public void getCurvedTime() {
        AccelerateDecelerateInterpolator accelerateDecelerateInterpolator = new AccelerateDecelerateInterpolator();

        assertEquals(0,accelerateDecelerateInterpolator.getCurvedTime(0),0.0);
    }
}