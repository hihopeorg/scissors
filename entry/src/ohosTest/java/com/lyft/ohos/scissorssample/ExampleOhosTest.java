package com.lyft.ohos.scissorssample;

import com.lyft.ohos.scissors2.CropView;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.aafwk.ability.delegation.IAbilityDelegator;
import ohos.utils.net.Uri;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertEquals;

public class ExampleOhosTest {
    private static IAbilityDelegator sAbilityDelegator = AbilityDelegatorRegistry.getAbilityDelegator();
    MainAbility ability = EventHelper.startAbility(MainAbility.class);
    CropView cropView = ability.cropView;

    @Test
    public void testCropArea() throws InterruptedException {
        EventHelper.waitForActive(ability, 5);
        Thread.sleep(1000);

        ability.getUITaskDispatcher().asyncDispatch(new Runnable() {
            @Override
            public void run() {
                cropView.setPixelMap(ResourceTable.Media_test);
            }
        });

        Thread.sleep(2000);
        EventHelper.triggerClickEvent(ability,ability.ratio_fab);
        assertEquals(1.0,cropView.getViewportRatio(),0.0);
    }

    @Test
    public void testCrop() throws InterruptedException {
        Thread.sleep(1000);
        EventHelper.inputSwipe(ability,500,1500,500,500,2000);
        Thread.sleep(1000);
        assertEquals(1,cropView.getViewportRatio(),0.0);
    }

    @Test
    public void testCropAreaSwipe() throws InterruptedException {
        Thread.sleep(1000);
        EventHelper.triggerClickEvent(ability,ability.crop_fab);
        assertEquals(true,sAbilityDelegator.getCurrentTopAbility()instanceof MainAbility);
    }


}