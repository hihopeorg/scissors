package com.lyft.ohos.scissorssample.junit;

import com.lyft.ohos.scissors2.util.DecelerateInterpolator;
import org.junit.Test;
import static org.junit.Assert.*;
public class DecelerateInterpolatorTest {

    @Test
    public void getCurvedTime() {
        DecelerateInterpolator decelerateInterpolator = new DecelerateInterpolator();
        assertEquals(0,decelerateInterpolator.getCurvedTime(0),0.0);
    }
}